{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedRecordDot    #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.SingletScalar where

import           Blocks                        (Coordinate (ZZb), CrossingMat,
                                                DerivMultiplier (..),
                                                Derivative, TaylorCoeff (..),
                                                Taylors, crossOdd, zzbTaylors)
import qualified Blocks                        as Blocks
import qualified Blocks.ScalarBlocks           as SB
import           Blocks.ScalarBlocks.Build     (scalarBlockBuildLink)
import           Bootstrap.Bounds              (FourPointFunctionTerm,
                                                HasRep (..), OPECoefficient,
                                                Spectrum (..), crossingMatrix,
                                                deltaGap, derivsVec, listDeltas,
                                                mapBlocks, opeCoeffIdentical_,
                                                runTagged)
import qualified Bootstrap.Bounds              as Bounds
import           Bootstrap.Build               (FetchConfig (..),
                                                SomeBuildChain (..), noDeps)
import qualified Bootstrap.Math.DampedRational as DR
import           Bootstrap.Math.FreeVect       (FreeVect, vec)
import           Bootstrap.Math.Linear.Literal (fromM, toV)
import           Bootstrap.Math.VectorSpace    (zero)
import           Control.Monad.IO.Class        (liftIO)
import           Data.Aeson                    (FromJSON, ToJSON)
import           Data.Binary                   (Binary)
import           Data.Data                     (Typeable)
import           Data.Functor.Compose          (Compose (..))
import           Data.Reflection               (Reifies, reflect)
import           Data.Tagged                   (Tagged (..), untag)
import           Data.Vector                   (Vector)
import           GHC.Generics                  (Generic)
import           GHC.TypeNats                  (KnownNat)
import           Hyperion                      (Dict (..), Static (..), cPtr)
import           Hyperion.Bootstrap.Bound      (BuildInJob,
                                                SDPFetchBuildConfig (..),
                                                ToSDP (..), blockDir)
import           Linear.V                      (V)
import qualified SDPB

data ExternalOp s = Phi
  deriving (Show, Eq, Ord, Enum, Bounded)

instance (Reifies s Rational) => HasRep (ExternalOp s) (SB.ScalarRep 3) where
  rep x@Phi = SB.ScalarRep $ reflect x

data SingletScalar = SingletScalar
  { deltaPhi    :: Rational
  , spectrum    :: Spectrum Int
  , objective   :: Objective
  , spins       :: [Int]
  , blockParams :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | TwistGap Int
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

crossingEquations :: forall s a b .
     FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEquations g0 = toV
  (zzbTaylors crossOdd, g0 Phi Phi Phi Phi (SB.Standard4PtStruct, SChannel))

singletScalarDerivsVec :: SingletScalar -> V 1 (Vector (TaylorCoeff (Derivative 'ZZb)))
singletScalarDerivsVec i = fmap ($ nmax) (derivsVec crossingEquations)
  where
    nmax = (blockParams i).nmax

singletScalarCrossingMat
  :: forall j s a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3) a)
  -> Tagged s (CrossingMat j 1 (SB.ScalarBlock 3) a)
singletScalarCrossingMat channel =
  pure $ mapBlocks SB.ScalarBlock $
  crossingMatrix channel (crossingEquations @s)

internalMat
  :: forall s a . (Reifies s Rational, Fractional a, Eq a)
  => SB.SymTensorRep 3
  -> Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
internalMat internalRep = singletScalarCrossingMat $ toV $
  opeCoeffIdentical_ (Phi @s) internalRep (vec ())

identityMat
  :: forall a s . (Fractional a, Eq a, Reifies s Rational)
  => Tagged s (CrossingMat 1 1 (SB.ScalarBlock 3) a)
identityMat = singletScalarCrossingMat (toV identityOpe)
  where
    identityRep = SB.SymTensorRep (Blocks.Fixed 0) 0
    identityOpe (o1 :: ExternalOp s) o2
      | o1 == o2  = vec (SB.Standard3PtStruct (rep o1) (rep o2) identityRep)
      | otherwise = zero

getInternalBlockVec
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => SB.SymTensorRep 3
  -> SingletScalar
  -> f (DR.DampedRational (Blocks.BlockBase (SB.ScalarBlock 3) a) Vector a)
getInternalBlockVec intRep i =
  fmap (DR.mapNumerator (fromM . getCompose)) $
  Bounds.getContinuumMat (blockParams i) (singletScalarDerivsVec i) $
  runTagged (deltaPhi i) $
  internalMat intRep

bulkConstraints
  :: forall a s m.
     ( RealFloat a, Binary a, Typeable a
     , Reifies s Rational
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     , Applicative m
     )
  => SingletScalar
  -> Tagged s [SDPB.PositiveConstraint m a]
bulkConstraints i@SingletScalar{..} = pure $ do
  l <- filter even spins
  (delta, range) <- listDeltas l spectrum
  let intRep = SB.SymTensorRep delta l
  pure $ Bounds.bootstrapConstraint blockParams dv range $ untag @s (internalMat intRep)
  where
    dv = singletScalarDerivsVec i

singletScalarSDP
  :: forall a m. ( RealFloat a, Applicative m, Binary a, Typeable a
     , Blocks.BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => SingletScalar
  -> SDPB.SDP m a
singletScalarSDP i@SingletScalar{..} = runTagged deltaPhi $ do
    bulk   <- bulkConstraints i
    unit   <- identityMat
    let dv = singletScalarDerivsVec i
        (cons, obj, norm) = case objective of
          Feasibility ->
            ( bulk
            , zero `asTypeOf` unit
            , Bounds.bootstrapNormalization blockParams dv unit
            )
          TwistGap l ->
            let
              gapRep :: SB.SymTensorRep 3 = SB.SymTensorRep (deltaGap spectrum l) l
              slopeVec = fmap (DR.evalDerivative 0) (getInternalBlockVec gapRep i)
              -- | Set the slope of the functional at gapRep to 1
              slopeNorm = SDPB.Normalization ("slope_" <> Bounds.tshowHash gapRep) slopeVec
            in
              ( bulk
              , unit
              , slopeNorm
              )
    return $ SDPB.SDP
      { SDPB.objective     = Bounds.bootstrapObjective blockParams dv obj
      , SDPB.normalization = norm
      , SDPB.positiveConstraints      = cons
      }

instance ToSDP SingletScalar where
  type SDPFetchKeys SingletScalar = '[ SB.BlockTableKey ]
  toSDP = singletScalarSDP

instance SDPFetchBuildConfig SingletScalar where
  sdpFetchConfig _ _ boundFiles =
    liftIO . SB.readBlockTable (blockDir boundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig boundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig boundFiles False

instance Static (Binary SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (Show SingletScalar)                where closureDict = cPtr (static Dict)
instance Static (ToSDP SingletScalar)               where closureDict = cPtr (static Dict)
instance Static (ToJSON SingletScalar)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig SingletScalar) where closureDict = cPtr (static Dict)
instance Static (BuildInJob SingletScalar)          where closureDict = cPtr (static Dict)
