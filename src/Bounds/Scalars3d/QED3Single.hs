{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE OverloadedRecordDot    #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.QED3Single where

import           Blocks                                      (BlockFetchContext,
                                                              Coordinate (ZZb),
                                                              CoordinateDir (CrossingDir),
                                                              CrossingMat,
                                                              Derivative (..),
                                                              Sign (..),
                                                              TaylorCoeff (..),
                                                              Taylors,
                                                              crossEven,
                                                              crossOdd,
                                                              unzipBlock,
                                                              zzbTaylors,
                                                              zzbTaylorsAll)
import qualified Blocks                                      as Blocks
import qualified Blocks.ScalarBlocks                         as SB
import           Blocks.ScalarBlocks.Build                   (scalarBlockBuildLink)
import qualified Bootstrap.Bounds.BootstrapSDP               as BSDP
import           Bootstrap.Bounds                            (BoundDirection,
                                                              FourPointFunctionTerm,
                                                              HasRep (..),
                                                              OPECoefficient,
                                                              OPECoefficientExternal,
                                                              ThreePointStructure (..),
                                                              boundDirSign,
                                                              crossingMatrix,
                                                              crossingMatrixExternal,
                                                              derivsVec, map4pt,
                                                              mapBlocksFreeVect,
                                                              opeCoeffExternalSimple,
                                                              opeCoeffGeneric_,
                                                              opeCoeffIdentical_,
                                                              runTagged)
import           Bootstrap.Bounds.Spectrum                   (DeltaRange,
                                                              Spectrum,
                                                              listDeltas)
import           Bootstrap.Build                             (FetchConfig (..),
                                                              SomeBuildChain (..),
                                                              noDeps)
import           Bootstrap.Math.FreeVect                     (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect                     as FV
import           Bootstrap.Math.Linear                       (bilinearPair, toV)
import qualified Bootstrap.Math.Linear                       as L
import           Bootstrap.Math.VectorSpace                  (zero, (*^))
import qualified Bootstrap.Math.VectorSpace                  as VS
import           Bounds.Scalars3d.QED3Rep                    (ON3PtStruct (..),
                                                              ON4PtStruct (..),
                                                              ONRep (..),
                                                              evalONBlock,
                                                              fromRepName)
import           Bounds.Scalars3d.SomeCrossingEquations      ( SomeV (..)
                                                             , SomeCrossingMat (..)
                                                             , (/:)
                                                             , (/:!)
                                                             , (/@))
import           Control.Monad.IO.Class                      (liftIO)
import           Data.Aeson                                  (FromJSON,
                                                              FromJSONKey,
                                                              ToJSON, ToJSONKey)
import           Data.Binary                                 (Binary)
import           Data.Data                                   (Typeable)
import           Data.Functor.Compose                        (Compose (..))
import           Data.List                                   (intercalate)
import qualified Data.Map.Strict                             as Map
import           Data.Matrix.Static                          (Matrix)
import qualified Data.Matrix.Static                          as M
import           Data.Proxy                        (Proxy (..))
import           Data.Reflection                             (Reifies, reflect)
import           Data.Tagged                                 (Tagged)
import           Data.Traversable                            (for)
import           Data.Vector                                 (Vector)
import qualified Data.Vector                                 as DV
import           GHC.Generics                                (Generic)
import           GHC.TypeNats                                (KnownNat, natVal)
import           Hyperion                                    (Dict (..),
                                                              Static (..), cPtr)
import           Hyperion.Bootstrap.Bound                    (BuildInJob,
                                                              SDPFetchBuildConfig (..),
                                                              ToSDP (..),
                                                              blockDir)
import           Linear.V                                    (V, toVector)
import qualified SDPB

data ExternalOp s = M1
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaM1    :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type ONRep62 = (ONRep 6, ONRep 2)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (SB.ScalarRep 3, ONRep62) where
  rep x@M1    = (SB.ScalarRep $ deltaM1    (reflect x), (ON $ toV (2,0,0,0,0,0), ON $ toV (2,0)))

-- | adapted from O(N)
instance ThreePointStructure
  (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2))
  ()
  (SB.ScalarRep 3, ONRep62)
  (SB.SymTensorRep 3, ONRep62) where
  makeStructure (l1, (r1,p1)) (l2, (r2,p2)) (l3, (r3,p3)) () =
    (makeStructure l1 l2 l3 (), (ON3PtStruct r1 r2 r3, ON3PtStruct p1 p2 p3))

instance ThreePointStructure
  (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2))
  ()
  (SB.ScalarRep 3, ONRep62)
  (SB.ScalarRep 3, ONRep62) where
  makeStructure (l1, (r1,p1)) (l2, (r2,p2)) (l3, (r3,p3)) () =
    (makeStructure l1 l2 l3 (), (ON3PtStruct r1 r2 r3, ON3PtStruct p1 p2 p3))

data ChannelType = ChannelType Int ONRep62
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

data QED3Single = QED3Single
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [Int]
  , blockParams  :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | StressTensorOPEBound BoundDirection
  | GFFNavigator
  | ExternalOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Channel b where
  GeneralChannel    :: SB.SymTensorRep 3 -> ONRep62 -> Channel (SB.ScalarBlock 3)
  IdentityChannel           :: Channel (SB.ScalarBlock 3) -- TODO: Check
  StressTensorChannel       :: Channel (SB.ScalarBlock 3)
  FlavorCurrentChannel      :: Channel (SB.ScalarBlock 3)
  TopologicalCurrentChannel :: Channel (SB.ScalarBlock 3)
  ExternalOpChannel         :: Channel (SB.ScalarBlock 3)

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel (SB.ScalarBlock 3) -- ^ The associated Channel
    -> BulkConstraint

-- | Crossing equation for a four-point function of scalars (copied from O(N))
crossingEqSSSS
  :: forall s a b .
     ExternalOp s
  -> Sign 'CrossingDir
  -> FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, Blocks.DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEqSSSS op sign g = toV --sign is FLAVOR sign
  (zzbTaylors (sign<>crossOdd), g op op op op (SB.Standard4PtStruct, Blocks.SChannel))

-- | Crossing equations of the GNY model with N /= 1 (adapted from GNY)
crossingEqsQED3Single
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s)
     ((SB.Standard4PtStruct, Blocks.DerivMultiplier), (ON4PtStruct 6, ON4PtStruct 2)) b a
  -> V 18 (Taylors 'ZZb, FreeVect b a)
crossingEqsQED3Single g = --TODO: check if any of these are redundant
  foldVecs [ crossingEqSSSS M1 (crossSign q6 <> crossSign q2) (map4pt (,(q6,q2)) g)
           | q6 <- [QPlus, QMinus, Qt, Qnt, QnPlus, QnMinus]
           , q2 <- [QPlus, QMinus, Qt] -- DO THESE APPEAR IN N=2?
           --, q2 <- [QPlus, QMinus, Qt, Qnt, QnPlus, QnMinus] -- DO THESE APPEAR IN bN=2?
           ]
  where
    -- NOTE: foldVecs does not incorporate size constraints!!!
    foldVecs = L.fromRawVector . foldr1 (<>) . map toVector
    crossSign QMinus  = crossOdd
    crossSign QnMinus = crossOdd
    crossSign _       = crossEven

derivsVecQED3Single :: QED3Single -> V 18 (Vector (TaylorCoeff (Derivative 'ZZb)))
derivsVecQED3Single QED3Single{..} =
  fmap ($ nmax) (derivsVec crossingEqsQED3Single)
  where
    nmax = blockParams.nmax

crossingMatQED3Single
  :: forall s a. (Fractional a, Eq a, RealFloat a)
  => SomeV (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a)
  -> Tagged s (SomeCrossingMat 18 (SB.ScalarBlock 3) a)
crossingMatQED3Single (SomeV (v :: V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a))) =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix @j v (crossingEqsQED3Single @s)
  where
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2

mat
  :: forall b s a . (Reifies s ExternalDims, Fractional a, RealFloat a, Eq a)
  => Channel b
  -> Tagged s (SomeCrossingMat 18 b a)

mat (GeneralChannel iConfRep rep62) =
  crossingMatQED3Single $ SomeV $ toV @1
  ( opeCoeffIdentical_ M1 (iConfRep, rep62) (vec ()) )


mat IdentityChannel =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix @1 (toV identityOpe) (crossingEqsQED3Single @s)
  where
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2
    identityOpe o1 o2
      | o1 == o2  = vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep $
                     SB.SymTensorRep (Blocks.Fixed 0) 0
        on62Struct = ( ON3PtStruct on6 on6 (ON zero)
                     , ON3PtStruct on2 on2 (ON zero) )

mat StressTensorChannel =
  crossingMatQED3Single $ SomeV $ toV @1 stressTensorOpe
  where
    stressTensorRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 2
    stressTensorOpe o1 o2
      | o1 == o2  =
        fromRational (SB.scalarDelta confRep) *^ vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep stressTensorRep
        on62Struct = ( ON3PtStruct on6 on6 (ON zero)
                     , ON3PtStruct on2 on2 (ON zero) )

mat FlavorCurrentChannel =
  mat (GeneralChannel
        (SB.SymTensorRep (Blocks.RelativeUnitarity 0) 1)
        (ON $ toV (1,1,0,0,0,0), ON zero) )

mat TopologicalCurrentChannel =
  mat (GeneralChannel
        (SB.SymTensorRep (Blocks.RelativeUnitarity 0) 1)
        (ON zero, ON $ toV (1,1)) )

mat ExternalOpChannel =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsQED3Single @s) [M1]
  where
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2
    opeCoefficients :: V 1 (OPECoefficientExternal (ExternalOp s)
                            (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a)
    opeCoefficients = toV (opeCoeffExternalSimple M1 M1 M1 (vec ()))

getExternalMat
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => QED3Single
  -> f (Matrix 1 1 (Vector a))
getExternalMat qed3@QED3Single{..} =
  BSDP.getIsolatedMat blockParams (derivsVecQED3Single qed3)
  /:! runTagged externalDims (mat ExternalOpChannel)


bulkConstraints :: QED3Single -> [BulkConstraint]
bulkConstraints QED3Single{..} = do
  (o6rep, o2rep, signTest) <- repFilterTable
  l <- filter signTest spins
  (delta, range) <- listDeltas (ChannelType l (o6rep, o2rep)) spectrum
  let iConformalRep = SB.SymTensorRep delta l
  pure $ BulkConstraint range $ GeneralChannel iConformalRep (o6rep, o2rep)


repFilterTable :: [(ONRep 6, ONRep 2, Int -> Bool)]
repFilterTable = withSign -- ++ withoutSign
  where
    filterBySign :: Int -> Int -> Bool
    filterBySign   1  = even
    filterBySign (-1) = not . even
    filterBySign   0  = (\_ -> True)
    filterBySign   _  = undefined

    signOf :: String -> Int
    signOf "S"   = 1
    signOf "V"   = 0
    signOf "A2"  = -1
    signOf "A2V" = 0
    signOf "T2"  = 1
    signOf "T3"  = 0
    signOf "T4"  = 1
    signOf "B"   = 1
    signOf "H"   = -1
    signOf _     = undefined

    withSign = do
      o6name <- ["S", "A2", "T2", "T4", "B", "H"]
      o2name <- ["S", "A2",       "T4"]
      pure (fromRepName o6name, fromRepName o2name,
            filterBySign (signOf o6name * signOf o2name))

--    withoutSign = do
--      o6name <- ["V", "A2V", "T3"]
--      o2name <- ["V",        "T3"]
--      pure (fromRepName o6name, fromRepName o2name, filterBySign 0)

qed3SingleSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => QED3Single
  -> SDPB.SDP m a
qed3SingleSDP f@QED3Single{..} = runTagged externalDims $ do
  let dv = derivsVecQED3Single f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      (BSDP.bootstrapConstraint blockParams dv range /:) <$> mat c
  unit        <- mat IdentityChannel
  stress      <- mat StressTensorChannel
  flavCurrent <- mat FlavorCurrentChannel
  topoCurrent <- mat TopologicalCurrentChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv /: stress --Isolated stress
    flavCurrentCons = BSDP.isolatedConstraint blockParams dv /: flavCurrent
    topoCurrentCons = BSDP.isolatedConstraint blockParams dv /: topoCurrent
  (cons, obj, norm) <- case objective of
    Feasibility -> pure
      ( bulk ++ [ stressCons, flavCurrentCons, topoCurrentCons]
      , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` (id /:! unit)
      , BSDP.bootstrapNormalization blockParams dv /:! unit
      )
    _ -> undefined
    -- TODO: implement other constraints
  return $ SDPB.SDP
    { SDPB.objective           = obj
    , SDPB.normalization       = norm
    , SDPB.positiveConstraints = cons
    }

qed3ShowSDP :: QED3Single -> (String, [String])
qed3ShowSDP f@QED3Single{..} = runTagged externalDims $ do
  let dv = derivsVecQED3Single f
  bulks       <- for (bulkConstraints f) $
    \(BulkConstraint range c) -> mat c
  unit        <- mat IdentityChannel
  stress      <- mat StressTensorChannel
  flavCurrent <- mat FlavorCurrentChannel
  topoCurrent <- mat TopologicalCurrentChannel
  let allCons = bulks ++ [ stress, flavCurrent, topoCurrent ]
  return ( pPrintCrossingMat /: unit
         , (pPrintCrossingMat /:) <$> allCons
         )

pPrintCrossingMat
  :: forall j n a d . (KnownNat n, Show a, Eq a, Fractional a) => CrossingMat j n (SB.ScalarBlock d) a
  -> String
pPrintCrossingMat m' =
  ppList (ppList (ppList (ppFreeVect ppBlock))) vm
  where
    m = fmap getCompose (getCompose m')
    vm = DV.toList $ DV.generate (fromIntegral (natVal @n Proxy)) $
         \i -> M.toLists (fmap (\elt -> toVector elt DV.! i) m)
    ppList ppElt v = "{" ++ intercalate ", " (map ppElt v) ++ "}"
    ppFreeVect ppBasis f =
      if Map.null (FV.toMap f)
      then "0"
      else intercalate " + "
           [ showCoeff coeff ++ ppBasis b | (b, coeff) <- FV.toList f ]
    showCoeff c = if
      | c == 1 -> ""
      | c == -1 -> "-"
      | otherwise -> show c ++ "*"
    ppBlock b'@(SB.ScalarBlock b) = "F[" ++ intercalate ","
      [ ppScalar (SB.operator1 (Blocks.struct12 b))
      , ppScalar (SB.operator2 (Blocks.struct12 b))
      , ppScalar (SB.operator2 (Blocks.struct43 b))
      , ppScalar (SB.operator1 (Blocks.struct43 b))
      , ppSymTensor (SB.internalRep b')
      , ppChan (Blocks.fourPtFunctional b)
      ] ++ "]"
    ppScalar (SB.ScalarRep delta) = "phi[" ++ show @Double (fromRational delta) ++ "]"
    ppSymTensor (SB.SymTensorRep delta l) = "Op[" ++ ppDelta delta ++ "," ++ show l ++ "]"
    ppDelta (Blocks.Fixed d) = show @Double (fromRational d)
    ppDelta (Blocks.RelativeUnitarity x) = "tau[" ++ show @Double (fromRational x) ++ "]"
    ppChan (_,Blocks.SChannel) = "s"
    ppChan (_,Blocks.TChannel) = "t"


instance ToSDP QED3Single where
  type SDPFetchKeys QED3Single = '[ SB.BlockTableKey ]
  toSDP = qed3SingleSDP

instance SDPFetchBuildConfig QED3Single where
  sdpFetchConfig _ _ boundFiles =
    liftIO . SB.readBlockTable (blockDir boundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig boundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig boundFiles False

instance Static (Binary QED3Single)              where closureDict = cPtr (static Dict)
instance Static (Show QED3Single)                where closureDict = cPtr (static Dict)
instance Static (ToSDP QED3Single)               where closureDict = cPtr (static Dict)
instance Static (ToJSON QED3Single)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig QED3Single) where closureDict = cPtr (static Dict)
instance Static (BuildInJob QED3Single)          where closureDict = cPtr (static Dict)
