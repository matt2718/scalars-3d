{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE NamedFieldPuns         #-}
{-# LANGUAGE OverloadedRecordDot    #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.QED3Mixed where

import           Blocks                                      (BlockFetchContext,
                                                              Coordinate (ZZb),
                                                              CoordinateDir (CrossingDir),
                                                              CrossingMat,
                                                              Derivative (..),
                                                              Sign (..),
                                                              TaylorCoeff (..),
                                                              Taylors,
                                                              crossEven,
                                                              crossOdd,
                                                              unzipBlock,
                                                              zzbTaylors,
                                                              zzbTaylorsAll)
import qualified Blocks                                      as Blocks
import qualified Blocks.ScalarBlocks                         as SB
import           Blocks.ScalarBlocks.Build                   (scalarBlockBuildLink)
import qualified Bootstrap.Bounds.BootstrapSDP               as BSDP
import           Bootstrap.Bounds (BoundDirection, FourPointFunctionTerm, HasRep (..),
                                    OPECoefficient, OPECoefficientExternal,
                                    ThreePointStructure (..), boundDirSign,
                                    crossingMatrix, crossingMatrixExternal,
                                    derivsVec, map4pt, mapBlocksFreeVect,
                                    opeCoeffExternalSimple,
                                    opeCoeffGeneric_, opeCoeffIdentical_,
                                    runTagged)
import           Bootstrap.Bounds.Spectrum                   (DeltaRange,
                                                              Spectrum,
                                                              listDeltas)
import           Bootstrap.Build                             (FetchConfig (..),
                                                              SomeBuildChain (..),
                                                              noDeps)
import           Bootstrap.Math.FreeVect                     (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect                     as FV
import           Bootstrap.Math.Linear                       (bilinearPair, toV)
import qualified Bootstrap.Math.Linear                       as L
import           Bootstrap.Math.VectorSpace                  (zero, (*^))
import qualified Bootstrap.Math.VectorSpace                  as VS
import           Bounds.Scalars3d.QED3Rep                    (ON3PtStruct (..),
                                                              ON4PtStruct (..),
                                                              ONRep (..),
                                                              evalONBlock,
                                                              fromFirstTwoTuple,
                                                              smallRepProduct,
                                                              toFirstTwoTuple,
                                                              toRepName,
                                                              )
import qualified Bounds.Scalars3d.QED3Rep                    as Rep
import           Bounds.Scalars3d.SomeCrossingEquations      ( SomeV (..)
                                                             , SomeCrossingMat (..)
                                                             , (/:)
                                                             , (/:!)
                                                             , (/@))
import           Control.Monad.IO.Class                      (liftIO)
import           Data.Aeson                                  (FromJSON,
                                                              FromJSONKey,
                                                              ToJSON, ToJSONKey)
import           Data.Binary                                 (Binary)
import           Data.Data                                   (Typeable)
import           Data.Functor.Compose                        (Compose (..))
import           Data.List                                   (intercalate, union)
import qualified Data.Map.Strict                             as Map
import           Data.Matrix.Static                          (Matrix)
import qualified Data.Matrix.Static                          as M
import           Data.Proxy                        (Proxy (..))
import           Data.Ratio                                  (approxRational, numerator, denominator)
import           Data.Reflection                             (Reifies, reflect)
import           Data.Tagged                                 (Tagged)
import           Data.Traversable                            (for)
import           Data.Vector                                 (Vector)
import qualified Data.Vector                                 as DV
import           GHC.Generics                                (Generic)
import           GHC.TypeNats                                (KnownNat, natVal)
import           Hyperion                                    (Dict (..),
                                                              Static (..), cPtr)
import           Hyperion.Bootstrap.Bound                    (BuildInJob,
                                                              SDPFetchBuildConfig (..),
                                                              ToSDP (..),
                                                              blockDir)
import           Linear.V                                    (V, toVector)
import qualified SDPB

import Debug.Trace

-- import Bounds.Scalars3d.QED3Single (ONRep62)
type ONRep62 = (ONRep 6, ONRep 2)

data ExternalOp s = MHalf | M1
  deriving (Show, Eq, Ord, Enum, Bounded)

data ExternalDims = ExternalDims
  { deltaMHalf :: Rational
  , deltaM1    :: Rational
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

-- type ONRep62 = (ONRep 6, ONRep 2)

instance (Reifies s ExternalDims) => HasRep (ExternalOp s) (SB.ScalarRep 3, ONRep62) where
  rep x@MHalf = (SB.ScalarRep $ deltaMHalf (reflect x), (ON $ toV (1,0,0,0,0,0), ON $ toV (1,0)))
  rep x@M1    = (SB.ScalarRep $ deltaM1 (reflect x), (ON $ toV (2,0,0,0,0,0), ON $ toV (2,0)))

instance ThreePointStructure
  (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2))
  ()
  (SB.ScalarRep 3, ONRep62)
  (SB.SymTensorRep 3, ONRep62) where
  makeStructure (l1, (r1,p1)) (l2, (r2,p2)) (l3, (r3,p3)) () =
    (makeStructure l1 l2 l3 (), (ON3PtStruct r1 r2 r3, ON3PtStruct p1 p2 p3))

instance ThreePointStructure
  (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2))
  ()
  (SB.ScalarRep 3, ONRep62)
  (SB.ScalarRep 3, ONRep62) where
  makeStructure (l1, (r1,p1)) (l2, (r2,p2)) (l3, (r3,p3)) () =
    (makeStructure l1 l2 l3 (), (ON3PtStruct r1 r2 r3, ON3PtStruct p1 p2 p3))

data ChannelType = ChannelType Int ONRep62
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey)

data QED3Mixed = QED3Mixed
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum ChannelType
  , objective    :: Objective
  , spins        :: [Int]
  , blockParams  :: SB.ScalarBlockParams
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Objective
  = Feasibility
  | JtOPEBound BoundDirection
  --  | GFFNavigator
  --  | StressTensorOPEBound BoundDirection
  --  | ExternalOPEBound BoundDirection
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

data Channel b where
  GeneralChannel    :: SB.SymTensorRep 3 -> ONRep62 -> Channel (SB.ScalarBlock 3)
  IdentityChannel           :: Channel (SB.ScalarBlock 3)
  StressTensorChannel       :: Channel (SB.ScalarBlock 3)
  FlavorCurrentChannel      :: Channel (SB.ScalarBlock 3)
  TopologicalCurrentChannel :: Channel (SB.ScalarBlock 3)
  ExternalOpChannel         :: Channel (SB.ScalarBlock 3)

-- | Data needed for "bulk" positivity conditions not associated with
-- external operators, the identity or the stress tensor.
data BulkConstraint where
  BulkConstraint
    :: DeltaRange -- ^ Isolated or Continuum constraint
    -> Channel (SB.ScalarBlock 3) -- ^ The associated Channel
    -> BulkConstraint

-- | Crossing equation for a four-point function of scalars (copied from O(N))
crossingEqSSSS
  :: forall s a b .
     ExternalOp s
  -> Sign 'CrossingDir
  -> FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, Blocks.DerivMultiplier) b a
  -> V 1 (Taylors 'ZZb, FreeVect b a)
crossingEqSSSS op sign g = toV --sign is FLAVOR sign
  (zzbTaylors (sign<>crossOdd), g op op op op (SB.Standard4PtStruct, Blocks.SChannel))

crossingEqSSSS2
  :: forall s a b . (Ord b, Fractional a, Eq a)
  => ExternalOp s
  -> ExternalOp s
  -> Sign 'CrossingDir
  -> FourPointFunctionTerm (ExternalOp s) (SB.Standard4PtStruct, Blocks.DerivMultiplier) b a
  -> V 2 (Taylors 'ZZb, FreeVect b a)
crossingEqSSSS2 o1 o2 sign g = toV
  ( (zzbTaylors (sign<>crossOdd), gS o1 o2 o1 o2)
  , (zzbTaylorsAll, gS o2 o2 o1 o1 - gT o1 o2 o2 o1)
  )
  where
    gS a b c d = g a b c d (SB.Standard4PtStruct, Blocks.SChannel)
    gT a b c d = g a b c d (SB.Standard4PtStruct, Blocks.TChannel)

-- | Mixed crossing equations of QED3 for the correlators:
--  < MHalf MHalf MHalf MHalf >
--  < M1    M1    M1    M1    >
--  < MHalf M1    Mhalf M1    >
--  < MHalf Mhalf M1    M1    >
crossingEqsQED3Mixed
  :: forall n s a b . (Ord b, Fractional a, Eq a)
  => FourPointFunctionTerm (ExternalOp s)
     ((SB.Standard4PtStruct, Blocks.DerivMultiplier), (ON4PtStruct 6, ON4PtStruct 2)) b a
  -> V 39 (Taylors 'ZZb, FreeVect b a)
crossingEqsQED3Mixed g =
  foldVecs @1 @9  [ crossingEqSSSS MHalf
                    (crossSign q6 <> crossSign q2) (map4pt (,(q6,q2)) g)
                  | q6 <- [QPlus, QMinus, Qt]
                  , q2 <- [QPlus, QMinus, Qt] ]
  L.++
  foldVecs @1 @18 [ crossingEqSSSS M1
                    (crossSign q6 <> crossSign q2) (map4pt (,(q6,q2)) g)
                  | q6 <- [QPlus, QMinus, Qt, Qnt, QnPlus, QnMinus]
                  , q2 <- [QPlus, QMinus, Qt] ]
  L.++
  foldVecs @1 @6  [ toV (zzbTaylors (crossSign q6 <> crossSign q2 <> crossOdd)
                    , g MHalf M1 MHalf M1 ((SB.Standard4PtStruct, Blocks.SChannel),(q6,q2)))
                  | q6 <- [QPlus, QMinus, Qt]
                  , q2 <- [QPlus, QMinus] ] -- TODO: REDUNDANCIES?
  L.++ 
  foldVecs @1 @6  [ toV (zzbTaylorsAll
                    , g M1 M1 MHalf MHalf ((SB.Standard4PtStruct, Blocks.TChannel),(q6,q2)) 
                    - g MHalf M1 M1 MHalf ((SB.Standard4PtStruct, Blocks.SChannel),(q6,q2)))
                  | q6 <- [As, At, Au]
                  , q2 <- [As, Au] ]
  where
    -- NOTE: foldVecs does not incorporate size constraints
    foldVecs :: forall m n . (KnownNat m, KnownNat n)
             => [V m (Taylors 'ZZb, FreeVect b a)]
             -> V n (Taylors 'ZZb, FreeVect b a)
    foldVecs = L.fromRawVector . foldr1 (<>) . map toVector
    crossSign QMinus  = crossOdd
    crossSign QnMinus = crossOdd
    crossSign _       = crossEven

derivsVecQED3Mixed :: QED3Mixed -> V 39 (Vector (TaylorCoeff (Derivative 'ZZb)))
derivsVecQED3Mixed QED3Mixed{..} =
  fmap ($ nmax) (derivsVec crossingEqsQED3Mixed)
  where
    nmax = blockParams.nmax

crossingMatQED3Mixed
  :: forall s a. (Fractional a, Eq a, RealFloat a)
  => SomeV (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a)
  -> Tagged s (SomeCrossingMat 39 (SB.ScalarBlock 3) a)
crossingMatQED3Mixed (SomeV (v :: V j (OPECoefficient (ExternalOp s) (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a))) =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix @j v (crossingEqsQED3Mixed @s)
  where
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2
  
-- crossing matrices are different sizes if the rep appears in both correlators
data OPEPair =  MhMh | M1M1 | M1Mh
  deriving (Eq, Show, Enum)

opeContains :: OPEPair -> ONRep62 -> Bool
opeContains ope (o6rep, o2rep) =
  o6contains && o2contains
  where
    o6contains = elem o6rep $ fromFirstTwoTuple <$> case ope of
      MhMh -> [(0,0), (1,1), (2,0)]
      M1M1 -> [(0,0), (1,1), (2,0), (4,0), (2,2), (3,1)]
      M1Mh -> [(1,0), (2,1), (3,0)]
    o2contains = elem o2rep $ fromFirstTwoTuple <$> case ope of
      MhMh -> [(0,0), (1,1), (2,0)]
      M1M1 -> [(0,0), (1,1), (4,0)]
      M1Mh -> [(1,0), (3,0)]

-- Matrices for each channel
mat
  :: forall b s a . (Reifies s ExternalDims, Fractional a, RealFloat a)
  => Channel b
  -> Tagged s (SomeCrossingMat 39 b a)
mat (GeneralChannel iConfRep rep62)
  -- need 2x2 crossing matrix
  | (MhMh `opeContains` rep62) && (M1M1 `opeContains` rep62)
  = crossingMatQED3Mixed $ SomeV $ toV @2
    ( opeCoeffIdentical_ MHalf (iConfRep, rep62) (vec ())
    , opeCoeffIdentical_ M1    (iConfRep, rep62) (vec ())
    )
  -- just in MhMh
  | (MhMh `opeContains` rep62) && not (M1M1 `opeContains` rep62)
  = crossingMatQED3Mixed $ SomeV $ toV @1
    ( opeCoeffIdentical_ MHalf (iConfRep, rep62) (vec ()) )
  -- just in M1M1
  | not (MhMh `opeContains` rep62) && (M1M1 `opeContains` rep62)
  = crossingMatQED3Mixed $ SomeV $ toV @1
    ( opeCoeffIdentical_ M1 (iConfRep, rep62) (vec ()) )
  -- mixing
  | M1Mh `opeContains` rep62
  = crossingMatQED3Mixed $ SomeV $ toV @1
    ( opeCoeffGeneric_ M1 MHalf (iConfRep, rep62)
      (vec ()) ((-1)^l *^ vec ())
    )
  | otherwise = error ("Crossing rep does not appear in any OPEs " ++ show rep62)
  where
    --flavSign = if fst rep62 == fromFirstTwoTuple (2,1) then -1 else 1
    l = SB.symTensorSpin iConfRep

-- TODO: CHECK CHECK CHECK
mat IdentityChannel =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrix @1 (toV identityOpe) (crossingEqsQED3Mixed @s)
  where
    prefactor MHalf = 2 * sqrt 3
    prefactor M1    = 2 * sqrt 10
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2
    identityOpe o1 o2
      | o1 == o2  = prefactor o1 *^ vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep $
                     SB.SymTensorRep (Blocks.Fixed 0) 0
        on62Struct = ( ON3PtStruct on6 on6 (ON zero)
                     , ON3PtStruct on2 on2 (ON zero) )

mat FlavorCurrentChannel =
  crossingMatQED3Mixed $ SomeV $ toV @1 currentOpe
  where
    prefactor MHalf = 4
    prefactor M1    = 8 * sqrt 2
    currentRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 1
    currentOpe o1 o2
      | o1 == o2  = prefactor o1 *^ vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep currentRep
        on62Struct = ( ON3PtStruct on6 on6 (ON $ toV (1,1,0,0,0,0))
                     , ON3PtStruct on2 on2 (ON zero) )

mat TopologicalCurrentChannel =
  crossingMatQED3Mixed $ SomeV $ toV @1 currentOpe
  where
    prefactor MHalf = 4
    prefactor M1    = 8 * sqrt (10 / 3)
    currentRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 1
    currentOpe o1 o2
      | o1 == o2  = prefactor o1 *^ vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep currentRep
        on62Struct = ( ON3PtStruct on6 on6 (ON zero)
                     , ON3PtStruct on2 on2 (ON $ toV (1,1)) )
        o1Charge = case o1 of
          MHalf -> 1/2
          M1    -> 1

mat StressTensorChannel =
  crossingMatQED3Mixed $ SomeV $ toV @1 stressTensorOpe
  where
    prefactor MHalf = 4 * sqrt 6 -- = 2sqrt6 / (D_GFF = 1/2)
    prefactor M1    = 8 * sqrt 5
    stressTensorRep = SB.SymTensorRep (Blocks.RelativeUnitarity 0) 2
    stressTensorOpe o1 o2
      | o1 == o2  =
        prefactor o1 * fromRational (SB.scalarDelta confRep) *^ vec (confStruct, on62Struct)
      | otherwise = 0
      where
        (confRep, (on6, on2)) = rep @(ExternalOp s) o1
        confStruct = SB.Standard3PtStruct confRep confRep stressTensorRep
        on62Struct = ( ON3PtStruct on6 on6 (ON zero)
                     , ON3PtStruct on2 on2 (ON zero) )

mat ExternalOpChannel =
  pure $ SomeCrossingMat $ mapBlocksFreeVect (evalFlavorCoeff . unzipBlock) $
  crossingMatrixExternal opeCoefficients (crossingEqsQED3Mixed @s) [MHalf, M1]
  where
    evalFlavorCoeff (b,f) = evalProduct (unzipBlock f) *^ vec (SB.ScalarBlock b)
    evalProduct (f1, f2) = evalONBlock f1 * evalONBlock f2
    opeCoefficients :: V 1 (OPECoefficientExternal (ExternalOp s)
                            (SB.Standard3PtStruct 3, (ON3PtStruct 6, ON3PtStruct 2)) a)
    opeCoefficients = toV $
      (\o1 o2 o3 -> FV.mapBasis (makeStructure (rep o1) (rep o2) (rep o3)) $
                    case (o1, o2, o3) of
                      (MHalf, MHalf, M1   ) -> sqrt 8 *^ vec ()
                      (MHalf, M1   , MHalf) -> sqrt (40/3) *^ vec ()
                      (M1,    MHalf, MHalf) -> sqrt (40/3) *^ vec ()
                      _                     -> zero
      )
      --toV ( opeCoeffExternalSimple MHalf MHalf M1 (vec ()) )

getExternalMat
  :: (Blocks.BlockFetchContext (SB.ScalarBlock 3) a f, Applicative f, RealFloat a)
  => QED3Mixed
  -> f (Matrix 2 2 (Vector a))
getExternalMat qed3@QED3Mixed{..} =
  BSDP.getIsolatedMat blockParams (derivsVecQED3Mixed qed3)
  /:! runTagged externalDims (mat ExternalOpChannel)

bulkConstraints :: QED3Mixed -> [BulkConstraint]
bulkConstraints QED3Mixed{..} = do
  -- iterate over all rep pairs and all spins
  rep62@(o6rep, o2rep) <- repTable
  let signTest = case signOf o6rep * signOf o2rep of
        1  -> even
        -1 -> odd
        0  -> const True
  l <- filter signTest spins
  -- spectrum assumptions
  (delta, range) <- listDeltas (ChannelType l rep62) spectrum
  pure $ BulkConstraint range $ GeneralChannel (SB.SymTensorRep delta l) rep62
  where
    -- CHANGE THIS WHEN RESTRICTING TO SINGLE CORRELATORS
    repTable = doubleRepProduct vRep vRep `union`
               doubleRepProduct tRep tRep `union`
               doubleRepProduct tRep vRep
    vRep = (fromFirstTwoTuple (1,0), fromFirstTwoTuple (1,0))
    tRep = (fromFirstTwoTuple (2,0), fromFirstTwoTuple (2,0))
    doubleRepProduct (ra6,ra2) (rb6,rb2) =
      [ (r6, r2) | r6 <- smallRepProduct ra6 rb6
                 , r2 <- smallRepProduct ra2 rb2 ]
    signOf r =
      if even x && even y then 1
      else if odd x && odd y then -1
      else 0
      where (x,y) = toFirstTwoTuple r

qed3MixedSDP
  :: forall a m .
     ( Binary a, Typeable a, RealFloat a, Applicative m
     , BlockFetchContext (SB.ScalarBlock 3) a m
     )
  => QED3Mixed
  -> SDPB.SDP m a
qed3MixedSDP f@QED3Mixed{..} = runTagged externalDims $ do
  let dv = derivsVecQED3Mixed f
  bulk   <- for (bulkConstraints f) $
    \(BulkConstraint range c) ->
      (BSDP.bootstrapConstraint blockParams dv range /:) <$> mat c
  unit        <- mat IdentityChannel
  stress      <- mat StressTensorChannel
  flavCurrent <- mat FlavorCurrentChannel
  topoCurrent <- mat TopologicalCurrentChannel
  extMat      <- mat ExternalOpChannel
  let
    stressCons = BSDP.isolatedConstraint blockParams dv /: stress --Isolated stress
    flavCurrentCons = BSDP.isolatedConstraint blockParams dv /: flavCurrent
    topoCurrentCons = BSDP.isolatedConstraint blockParams dv /: topoCurrent
    extCons mLambda = case mLambda of
      Nothing     -> BSDP.isolatedConstraint blockParams dv /: extMat
      Just lambda -> undefined
  (cons, obj, norm) <- case objective of
    Feasibility -> pure
      ( bulk ++ [ stressCons, flavCurrentCons, topoCurrentCons, extCons Nothing ]
      , BSDP.bootstrapObjective blockParams dv $ zero `asTypeOf` (id /:! unit)
      , BSDP.bootstrapNormalization blockParams dv /:! unit
      )
    -- Modify the spectrum readout code if this ordering is ever changed! -Matt
    JtOPEBound dir -> pure
      ( bulk ++ [ stressCons, flavCurrentCons, extCons Nothing ]
      , BSDP.bootstrapObjective blockParams dv /:! unit
      , BSDP.bootstrapNormalization blockParams dv . (boundDirSign dir *^) /:!
        topoCurrent
      )
    _ -> undefined
  return $ SDPB.SDP
    { SDPB.objective           = obj
    , SDPB.normalization       = norm
    , SDPB.positiveConstraints = cons
    }

instance ToSDP QED3Mixed where
  type SDPFetchKeys QED3Mixed = '[ SB.BlockTableKey ]
  toSDP = qed3MixedSDP

instance SDPFetchBuildConfig QED3Mixed where
  sdpFetchConfig _ _ boundFiles =
    liftIO . SB.readBlockTable (blockDir boundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig boundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig boundFiles False

instance Static (Binary QED3Mixed)              where closureDict = cPtr (static Dict)
instance Static (Show QED3Mixed)                where closureDict = cPtr (static Dict)
instance Static (ToSDP QED3Mixed)               where closureDict = cPtr (static Dict)
instance Static (ToJSON QED3Mixed)              where closureDict = cPtr (static Dict)
instance Static (SDPFetchBuildConfig QED3Mixed) where closureDict = cPtr (static Dict)
instance Static (BuildInJob QED3Mixed)          where closureDict = cPtr (static Dict)


qed3ShowSDP :: QED3Mixed -> [(String, String)]
qed3ShowSDP f@QED3Mixed{..} = runTagged externalDims $ do
  let dv = derivsVecQED3Mixed f
  unit        <- mat IdentityChannel
  stress      <- mat StressTensorChannel
  flavCurrent <- mat FlavorCurrentChannel
  topoCurrent <- mat TopologicalCurrentChannel
  bulks       <- for (bulkConstraints f) $ \(BulkConstraint range c) -> mat c
  -- pretty printing of channel heading
  let showCh (BulkConstraint _ (GeneralChannel cRep (r6, r2)))
        = "{rep[" ++ toRepName r6 ++ "," ++ toRepName r2 ++ "]," ++
          show ((-1) ^ SB.symTensorSpin cRep) ++ "}" ++
          ", spin = " ++ show (SB.symTensorSpin cRep) ++
          case SB.symTensorDelta cRep of
            (Blocks.Fixed dim) -> ", FIXED Δ = " ++ show (fromRational dim)
            _ -> ""
  --- display current constraints separately
  return $ [ ("UNIT"               , pPrintCrossingMat dv /: unit)
           , ("STRESS"             , pPrintCrossingMat dv /: stress)
           , ("FLAVOR CURRENT"     , pPrintCrossingMat dv /: flavCurrent)
           , ("TOPOLOGICAL CURRENT", pPrintCrossingMat dv /: topoCurrent)
           ] ++ zip (map showCh (bulkConstraints f)) ((pPrintCrossingMat dv /:) <$> bulks)

pPrintCrossingMat
  :: forall j n a d . (KnownNat n, Show a, Eq a, RealFrac a)
  => V n (Vector (TaylorCoeff (Derivative 'ZZb)))
  -> CrossingMat j n (SB.ScalarBlock d) a
  -> String
pPrintCrossingMat dvec m' =
  ppList (\(v,sign) -> ppList (ppList (ppFreeVect (ppBlock sign))) v) (zip vm signList)
  where
    -- hleadingCoeffs = traceShow (fmap (\(x,y) -> even (x+y)) <$> coeffSets) $ head <$> coeffSets
    coeffSets = (fmap ((\(x,y) -> even (x+y) ) . Blocks.unDerivative . Blocks.unTaylorCoeff)
                 . DV.toList)
      <$> (DV.toList $ toVector dvec)
    signList = (\l -> if all id l then 1
                      else if not (any id l) then (-1)
                           else 0) <$> coeffSets
    m = fmap getCompose (getCompose m')
    vm = DV.toList $ DV.generate (fromIntegral (natVal @n Proxy)) $
         \i -> M.toLists (fmap (\elt -> toVector elt DV.! i) m)
    ppList ppElt v = "{" ++ intercalate ", " (map ppElt v) ++ "}"
    ppFreeVect ppBasis f =
      if Map.null (FV.toMap f)
      then "0"
      else intercalate " + "
           [ showCoeff coeff ++ ppBasis b | (b, coeff) <- FV.toList f ]
    showCoeff c = if
      | c == 1 -> ""
      | c == -1 -> "-"
      | otherwise -> showAsRat c ++ "*"
    ppBlock sign b'@(SB.ScalarBlock b) = (case sign of
                                            1 -> "H["
                                            0 -> "G["
                                            (-1) -> "F[")
      ++ intercalate ","
      [ ppScalar (SB.operator1 (Blocks.struct12 b))
      , ppScalar (SB.operator2 (Blocks.struct12 b))
      , ppScalar (SB.operator2 (Blocks.struct43 b))
      , ppScalar (SB.operator1 (Blocks.struct43 b))
      --, ppSymTensor (SB.internalRep b')
      --, ppChan (Blocks.fourPtFunctional b)
      ] ++ "]"
    ppScalar (SB.ScalarRep delta) = "phi[" ++ show @Double (fromRational delta) ++ "]"
    ppSymTensor (SB.SymTensorRep delta l) = "Op[" ++ ppDelta delta ++ "," ++ show l ++ "]"
    ppDelta (Blocks.Fixed d) = show @Double (fromRational d)
    ppDelta (Blocks.RelativeUnitarity x) = "tau[" ++ show @Double (fromRational x) ++ "]"
    ppChan (_,Blocks.SChannel) = "s"
    ppChan (_,Blocks.TChannel) = "t"
    
    showAsRat fl =
     let r = approxRational fl 0.00000001
     in show (numerator r) ++ "/" ++ show (denominator r)
