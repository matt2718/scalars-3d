{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE PolyKinds                 #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE InstanceSigs              #-}

module Bounds.Scalars3d.SomeCrossingEquations where

import           GHC.TypeNats       (KnownNat, sameNat, natVal)
import           Blocks.HasBlocks   (CrossingMat)
import           Data.Proxy         (Proxy (..))
import           Data.Type.Equality ((:~:) (..))
import           Linear.V           (V)


data SomeV a where
  SomeV :: forall n a. KnownNat n => V n a -> SomeV a

-- | We need a type which existentially quantifies over CrossingMat
data SomeCrossingMat n b a where
  SomeCrossingMat :: KnownNat j => CrossingMat j n b a -> SomeCrossingMat n b a

someCrossingMatComposeFixed :: forall j n b a x. (KnownNat j) => (CrossingMat j n b a -> x) -> SomeCrossingMat n b a -> x
someCrossingMatComposeFixed f (SomeCrossingMat (c :: CrossingMat j' n b a)) = case sameNat (Proxy :: Proxy j) (Proxy :: Proxy j') of
  Just Refl -> f c
  Nothing -> error $ "can't apply a dim " ++ show (natVal (Proxy :: Proxy j')) ++ " vector to a function that needs dim " ++ show (natVal (Proxy :: Proxy j))

(/:!) :: forall j n b a x. (KnownNat j) => (CrossingMat j n b a -> x) -> SomeCrossingMat n b a -> x
(/:!) = someCrossingMatComposeFixed
infixr 5 /:!

someCrossingMatCompose :: forall n b a x . (forall j . (KnownNat j) => CrossingMat j n b a -> x) -> SomeCrossingMat n b a -> x
someCrossingMatCompose f (SomeCrossingMat c) = f c

(/:) :: forall n b a x . (forall j . (KnownNat j) => CrossingMat j n b a -> x) -> SomeCrossingMat n b a -> x
(/:) = someCrossingMatCompose
infixr 5 /:

someCrossingMatMap :: forall n b a . (forall j . (KnownNat j) => CrossingMat j n b a -> CrossingMat j n b a) -> SomeCrossingMat n b a -> SomeCrossingMat n b a
someCrossingMatMap f (SomeCrossingMat c) = SomeCrossingMat (f c)

(/@) :: forall n b a . (forall j . (KnownNat j) => CrossingMat j n b a -> CrossingMat j n b a) -> SomeCrossingMat n b a -> SomeCrossingMat n b a
(/@) = someCrossingMatMap
infixl 5 /@
