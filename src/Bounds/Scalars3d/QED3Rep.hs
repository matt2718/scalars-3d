{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE PatternSynonyms        #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.QED3Rep where

import           Data.Aeson                        (FromJSON, ToJSON)
import           Data.Binary                       (Binary)
import           Data.Foldable                     as Foldable
import           Data.Proxy                        (Proxy (..))
import           Data.Reflection                   (Reifies, reflect)
import qualified Data.Vector                       as Vector
import           GHC.Generics                      (Generic)--, Generic1)
import           GHC.TypeNats                      (KnownNat, natVal)
import           Linear.V                          (V (..))--, fromVector)
import           Blocks                            (Block (..))
import           Bootstrap.Math.FreeVect           (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect           as FV
import qualified Bootstrap.Math.Linear             as L
import           Bootstrap.Math.Linear.Instances   ()
import           Bootstrap.Math.VectorSpace        ((*^), zero)

------------------ O(N) representation theory ---------------------

-- | Here, we only implement the O(N) representation theory needed for
-- this problem, which involves four-point functions of q=1/2,1 monopoles, which have
-- SO(6) reps of (1,1,0) and (2,2,0), respectively.

-- | O(N) representations in terms of their young diagrams
-- | The nth element is the length of the nth ROW of the diagram
data ONRep n = ON (V n Int)
  deriving (Show, Eq, Ord, Generic, Binary, Bounded, ToJSON, FromJSON)

-- instance ToJSON (V n Int) = 

--instance ToJSON (ONRep n)
--   toEncoding = genericToEncoding defaultOptions

-- instance FromJSON (ONRep n)

isEmptyOrZero :: (Num a, Eq a) => [a] -> Bool
isEmptyOrZero [] = True
isEmptyOrZero (0:xs) = isEmptyOrZero xs
isEmptyOrZero _ = False

toFirstTwoTuple :: (KnownNat n) => ONRep n -> (Int, Int)
toFirstTwoTuple (ON v) =
  case toList v of
    x1:x2:_  -> (x1,x2)
    _        -> undefined

fromFirstTwoTuple :: forall n . KnownNat n => (Int, Int) -> ONRep n
fromFirstTwoTuple (x1,x2) =
  ON $ L.fromRawVector $ Vector.fromList asList
  where
    vLen = fromIntegral $ natVal @n Proxy
    asList = x1 : x2 : replicate (vLen - 2) 0

toRepName :: (KnownNat n) => ONRep n -> String
toRepName r =
  case toFirstTwoTuple r of
    (0,0) -> "S"
    (1,0) -> "V"
    (1,1) -> "A2"
    (2,1) -> "A2V"
    (2,0) -> "T2"
    (3,0) -> "T3"
    (4,0) -> "T4"
    (2,2) -> "B"
    (3,1) -> "H"
    _ -> "???"

-- for backwards-compat
fromRepName :: (KnownNat n) => String -> ONRep n
fromRepName "S"   = fromFirstTwoTuple (0,0)
fromRepName "V"   = fromFirstTwoTuple (1,0)
fromRepName "A2"  = fromFirstTwoTuple (1,1)
fromRepName "A2V" = fromFirstTwoTuple (2,1)
fromRepName "T2"  = fromFirstTwoTuple (2,0)
fromRepName "T3"  = fromFirstTwoTuple (3,0)
fromRepName "T4"  = fromFirstTwoTuple (4,0)
fromRepName "B"   = fromFirstTwoTuple (2,2)
fromRepName "H"   = fromFirstTwoTuple (3,1)
fromRepName _ = undefined

-- | General O(N) flavor structures for a four-point function of monopoles,
-- in the crossing-definite basis in terms of the "simple basis."
-- Because Haskell needs labels for each structure, but each correlation
-- function has its own independent set of structures, we reuse our structures
-- for simplicity of coding and ease of consideration. The first three structures
-- are used by all correlators; the second three are only used for TTTT.
--
-- The first three terms of the simple basis are as follows, with v_{12} indicating
-- the contraction of the O1 and O2 polarization vectors i.e. v_{12} = (S_1 \cdot S_2).
-- Everything is written in the context of the VVVV correlator (i.e. 1st order in all the
-- polarization vectors), but we reuse these for the other correlators by simply adjusting
-- the power of each of the products.
--  Bs  = v_{12} v_{34}
--  Bt  = v_{13} v_{24}
--  Bu  = v_{14} v_{23}
--
-- The second three terms of the simple basis are only used for the TTTT correlator. The
-- notation of "nt" indicates pairings that are *not* the t-channel, e.g.. The advantage of
-- this notation is that Bnt goes to itself under (13) crossing, just like Bt.
--  Bnt = v_{12} v_{23} v_{34} v_{41}
--  Bnu = v_{13} v_{34} v_{42} v_{21}
--  Bns = v_{14} v_{42} v_{23} v_{31}
--
-- Then, the (13)-crossing-definite basis:
--  QPlus   = Bs + Bu   (even)
--  QMinus  = Bs - Bu   (odd)
--  Qt      = Bt        (even)
--  Qnt     = Bnt       (even)
--  QnPlus  = Bns + Bnu (even)
--  QnMinus = Bns - Bnu (odd)
--
-- FOR VTVT: SUBSET OF QPLUS, QMINUS, QT
--           
-- FOR TTVV: As = Bu, Au = Bs, At = Bt
-- for VTTV: As = Bs, Au = Bu, At = At
data ON4PtStruct n = QPlus | QMinus | Qt | Qnt | QnPlus | QnMinus | As | At | Au
  deriving (Eq, Ord, Show)

-- | For the SO6Reps we allow here, there is a unique three-point
-- structure for each allowed triplet of representations. Thus, the
-- 3-point structures are just labeled by the representations.
data ON3PtStruct n = ON3PtStruct (ONRep n) (ONRep n) (ONRep n)
  deriving (Eq, Ord, Show)

-- | Flavor block for the exchange of the given 'ONRep' in a
-- four-point function of ON vectors.
onVectorBlock
  :: forall n a . (KnownNat n, Fractional a, Ord a)
  => ONRep n
  -> FreeVect (ON4PtStruct n) a
onVectorBlock r
  | nGroup >= 2 = case op r of
      (0,0) -> 1/nGroup *^ v12_34 --fix
      (2,0) -> 1/2 *^ (v13_24 + v23_14) - 1/nGroup *^ v12_34
-- | Fixed a sign here, since the block should come from merging
-- (120) and (430)s
      (1,1) -> 1/2 *^ (v23_14 - v13_24) --NEG
      _               -> 0
  | otherwise = error "have not yet implemented for n<2"
  where
    -- | vij_kl represents v_{ij} v_{kl}. Here, they can be obtained
    -- by inverting the definitions of QPlus, QMinus, Q3.
    v12_34 = 1/2 *^ vec QPlus + 1/2 *^ vec QMinus
    v23_14 = 1/2 *^ vec QPlus - 1/2 *^ vec QMinus
    v13_24 = vec Qt
    nGroup = fromIntegral (natVal @n Proxy)
    op = toFirstTwoTuple

-- | Flavor block for the exchange of the given 'ONRep' in a
-- four-point function of ON tensors.
onTensorBlock
  :: forall n a . (KnownNat n, Fractional a, Ord a)
  => ONRep n
  -> FreeVect (ON4PtStruct n) a
onTensorBlock r
  | nGroup == 2 = case op r of
    (0,0) -> 2/((nGroup-1)*(nGroup+2)) *^ bs --fix
    (4,0) -> 1/2 *^ (bt + bu) - 1/nGroup *^ bs
-- | Fixed a sign here, since the block should come from merging
-- (120) and (430) -- double check this!
    (1,1) -> 1/2 *^ (bu - bt)
    _     -> 0
  | nGroup > 2 = case op r of
    (0,0) ->  2/((nGroup-1)*(nGroup+2)) *^ bs
    (2,0) ->  4*nGroup/((nGroup-2)*(nGroup+4)) *^ (1/2 *^ (bnt + bnu) - 1/nGroup *^ bs)
-- | Fixed a sign here, since the block should come from merging
-- (120) and (430) -- double check this! I think the signs "undo"
-- each other here
    (1,1) ->  2/(nGroup+2) *^ (bnt - bnu)
    (4,0) ->  2/3 *^ bns + 1/6 *^ (bu + bt) - 4/(3*(nGroup+4)) *^ (bnu + bnt - 1/(nGroup+2) *^ bs)
    (3,1) ->  -2/(nGroup+2) *^ (bnt - bnu) + 1/2 *^ (bu - bt) -- |ERROR? (flip first)
    (2,2) -> -2/3 *^ bns + 1/3 *^ (bu + bt) - 2/(3*(nGroup-2)) *^ (bnu + bnt - 1/(nGroup-1) *^ bs)
    _     -> 0
  | otherwise = error "have not yet implemented for n<2"
  where
    -- | following the conventions in the above comment
    bs  = 1/2 *^ vec QPlus  + 1/2 *^ vec QMinus
    bu  = 1/2 *^ vec QPlus  - 1/2 *^ vec QMinus
    bt  = vec Qt
    bns = 1/2 *^ vec QnPlus + 1/2 *^ vec QnMinus
    bnu = 1/2 *^ vec QnPlus - 1/2 *^ vec QnMinus
    bnt = vec Qnt
    nGroup = fromIntegral (natVal @n Proxy)
    op = toFirstTwoTuple

-- | corresponds to the VTTV block
onVTTVBlock
  :: forall n a . (KnownNat n, Fractional a, Ord a)
  => ONRep n
  -> FreeVect (ON4PtStruct n) a
onVTTVBlock r
  | nGroup == 2 = case op r of
    (1,0) -> bs
    -- | TODO: Double check this -- it might need a sign flip (OR FACTOR)
    (3,0) -> (bu - bs) --fix?
    _     -> 0
  | nGroup > 2 = case op r of
    (1,0) ->  2*nGroup/((nGroup-1)*(nGroup+2)) *^ bs
    (3,0) ->  1/3 *^ (bu + 2*^bt) - 4/(3*(nGroup+2)) *^ bs
    -- | TODO: Also check this
    (2,1) -> -(2/3 *^ (bt - bu) + 2/(3*(nGroup+2)) *^ bs)
    _     -> 0
  | otherwise = error "have not yet implemented for n<2"
  where
    -- | following the conventions in the above comment
    bs  = vec As
    bu  = vec Au
    bt  = vec At
    nGroup = fromIntegral (natVal @n Proxy)
    op = toFirstTwoTuple

-- | corresponds to the VTVT block, which is the same (at the level
--   of 4pt functions) as VTTV. The permutation is handled in the
--   3pt structs.

onVTVTBlock
  :: forall n a . (KnownNat n, Fractional a, Ord a)
  => ONRep n
  -> FreeVect (ON4PtStruct n) a
onVTVTBlock =
  FV.evalFreeVect changeBasis . onVTTVBlock
  where --TODO FIX FIX FIX
    nGroup = fromIntegral (natVal @n Proxy)
    -- As goes to Bs = 1/2 qplus + 1/2 qminus
    changeBasis As = 1/2 *^ vec QPlus + 1/2 *^ vec QMinus
    -- At goes to Bu = 1/2 qplus - 1/2 qminus
    changeBasis At = 1/2 *^ vec QPlus - 1/2 *^ vec QMinus
    -- Au goes to Bt = Qt, except when n==2 when Bt = Bs + Bu
    changeBasis Au
      | nGroup == 2 = vec QPlus
      | nGroup > 2 = vec Qt
      | otherwise = error "how did you get here??"
    changeBasis _ = undefined

-- | corresponds to the VVTT block
onVVTTBlock
  :: forall n a . (KnownNat n, RealFloat a)
  => ONRep n
  -> FreeVect (ON4PtStruct n) a
onVVTTBlock r -- double check, should this be identical to TTVV?
  | nGroup == 2 = case op r of
    (0,0) -> 1/2 *^ bs --fix
    -- | TODO: Double check this -- it might need a sign flip
    (1,1) -> bu - 1/2 *^ bs
    _     -> 0
  | nGroup > 2 = case op r of
    (0,0) ->  sqrt (2 / (nGroup*(nGroup-1)*(nGroup+2))) *^ bs
    (2,0) ->  - (2 * (sqrt (nGroup/((nGroup+4)*(nGroup-2))))) *^ (1/2 *^ (bt + bu) - 1/nGroup *^ bs)--NEG
    (1,1) -> 1 / sqrt (nGroup+2) *^ (bu - bt)
    _     -> 0
  | otherwise = error "have not yet implemented for n<2"
  where
    -- | following the conventions in the above comment
    bs  = vec Au
    bu  = vec As
    bt  = vec At
    nGroup = fromIntegral (natVal @n Proxy)
    op = toFirstTwoTuple

-- | Evaluate the given O(N) flavor block to obtain a number
evalONBlock :: (KnownNat n, RealFloat a) => Block (ON3PtStruct n) (ON4PtStruct n) -> a
evalONBlock (Block (ON3PtStruct r1 r2 r) (ON3PtStruct r4 r3 r') f)
  | r /= r' = 0
  | otherwise = FV.coeff f $ case (op r1, op r2, op r3, op r4) of
      ((1,0), (1,0), (1,0), (1,0)) -> onVectorBlock r
      ((2,0), (2,0), (2,0), (2,0)) -> onTensorBlock r
      ((1,0), (1,0), (2,0), (2,0)) -> onVVTTBlock   r
      ((2,0), (2,0), (1,0), (1,0)) -> onVVTTBlock   r
      ((1,0), (2,0), (1,0), (2,0)) -> onVTVTBlock   r
      ((2,0), (1,0), (2,0), (1,0)) -> onVTVTBlock   r
      ((1,0), (2,0), (2,0), (1,0)) -> onVTTVBlock   r
      ((2,0), (1,0), (1,0), (2,0)) -> onVTTVBlock   r
      _ -> 0
    where
      --onVTVTBlock'' x = case onVTVTBlock' x 
      op = toFirstTwoTuple

-- | Given two V or T2 representations, return a list of the representations
-- appearing in their product
smallRepProduct
  :: forall n . (KnownNat n)
  => ONRep n -> ONRep n -> [ONRep n]
smallRepProduct r1 r2
  | not (r1 `elem` allowedFactors && r2 `elem` allowedFactors) =
    error "not implemented"
  | nGroup <  2 = error "not implemented"
  | nGroup == 2 = fromFirstTwoTuple <$> case (op r1, op r2) of
      ((1,0), (1,0)) -> [(0,0), (2,0), (1,1)]
      ((1,0), (2,0)) -> [(1,0), (3,0)]
      ((2,0), (1,0)) -> [(1,0), (3,0)]
      ((2,0), (2,0)) -> [(0,0), (1,1), (4,0)]
      _ -> undefined
  | nGroup >  2 = fromFirstTwoTuple <$> case (op r1, op r2) of
      ((1,0), (1,0)) -> [(0,0), (2,0), (1,1)]
      ((1,0), (2,0)) -> [(1,0), (2,1), (3,0)]
      ((2,0), (1,0)) -> [(1,0), (2,1), (3,0)]
      ((2,0), (2,0)) -> [(0,0), (2,0), (4,0), (2,2), (1,1), (3,1)]
      _ -> undefined
  where
    nGroup = fromIntegral (natVal @n Proxy)
    allowedFactors = fromFirstTwoTuple <$> [(1,0), (2,0)]
    op = toFirstTwoTuple
