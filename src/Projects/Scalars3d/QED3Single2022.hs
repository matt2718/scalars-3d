{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeApplications      #-}

module Projects.Scalars3d.QED3Single2022 where

import qualified Blocks                             as Blocks
import qualified Blocks.ScalarBlocks                as SB
import           Bootstrap.Bounds.Spectrum          (deltaGaps, setGaps, setTwistGap,
                                                     unitarySpectrum)
import           Bootstrap.Math.Linear              (fromRows, fromV, toV)
import qualified Bootstrap.Math.Linear              as L
--import           Bootstrap.Math.VectorSpace         (zero, (*^))
import qualified Bootstrap.Math.VectorSpace         as VS
import           Bounds.Scalars3d.QED3Rep           (ONRep (..), fromFirstTwoTuple)
import qualified Bounds.Scalars3d.QED3Single        as Q3S
import           Control.Monad                      (forM_, void, when)
import           Control.Monad.IO.Class             (MonadIO, liftIO)
import           Control.Monad.Reader               (asks, local)
import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.List.Split                    (chunksOf)
import qualified Data.Map.Strict                    as Map
import           Data.Maybe                         (fromMaybe, listToMaybe)
import           Data.Text                          (Text, pack, splitOn, strip,
                                                     unpack)
import qualified Data.Text
import           Data.Time.Clock                    (NominalDiffTime)
import           Hyperion
import           Bootstrap.Math.AffineTransform     (AffineTransform (..), apply)
import           Hyperion.Bootstrap.Bound           (Bound (..),
                                                     BoundFiles (..))
import qualified Hyperion.Bootstrap.Bound           as Bound
import           Hyperion.Bootstrap.DelaunaySearch  (DelaunayConfig (..),
                                                     delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main            (unknownProgram)
import           Hyperion.Bootstrap.OPESearch       (BilinearForms (..),
                                                     OPESearchConfig (..),
                                                     TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch       as OPE
import qualified Hyperion.Bootstrap.Params          as Params
import qualified Hyperion.Bootstrap.SDPDeriv        as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.BFGS   as BFGS
import qualified Hyperion.Bootstrap.SDPDeriv.Newton as Newton
import qualified Hyperion.Database                  as DB
import qualified Hyperion.Log                       as Log
import qualified Hyperion.Slurm                     as Slurm
import           Hyperion.Util                      (hour, minute)
import           Linear.V                           (V)
import           Linear.Vector                      (zero)
import           Numeric                            (readFloat, readSigned)
import           Numeric.Rounded                    (Rounded, RoundingMode (..))
import           Projects.Scalars3d.Defaults        (defaultBoundConfig,
                                                     defaultDelaunayConfig)
import           Projects.Scalars3d.Delaunay2       (delaunaySearchCubePersistent)
import           Projects.Scalars3d.QED3SingleData  (getSSB2d)
import           SDPB                               (Params (..))
import qualified SDPB
import           System.Directory                   (removePathForcibly)

remoteComputeQ3SBoundMulti :: [Bound Int Q3S.QED3Single] -> Cluster ()
remoteComputeQ3SBoundMulti bounds = do
  workDir <- newWorkDir (head bounds)
  jobTime <- asks (Slurm.time . clusterJobOptions)
  remoteEvalJob $
    static compute `ptrAp` cPure jobTime `cAp` cPure bounds `cAp` cPure workDir
  where
    computationsMap :: DB.KeyValMap (Bound Int Q3S.QED3Single) SDPB.Output
    computationsMap = DB.KeyValMap "computations"
    compute jobTime bounds' workDir' = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime) (solverParams (head bounds'))
      forM_ (zip bounds' [1 :: Int ..]) $ \(bound,i) ->
        DB.lookup computationsMap bound >>= \case
        Just _  -> pure ()
        Nothing -> do
          Log.text $ "Computing bound " <> Log.showText i <> " of " <> Log.showText (length bounds') <> "."
          let boundFiles = Bound.defaultBoundFiles workDir'
          result <- Bound.computeClean' (bound { solverParams = solverParams' }) boundFiles
          when (SDPB.isFinished result) $
            DB.insert computationsMap bound result
          liftIO $ mapM_ removePathForcibly [checkpointDir boundFiles, outDir boundFiles]

mkChannel :: (Int,Int) -> (Int,Int) -> Int -> Q3S.ChannelType
mkChannel o6rep o2rep spin =
  Q3S.ChannelType spin
  ( fromFirstTwoTuple o6rep
  , fromFirstTwoTuple o2rep
  )

q3SFeasibleBasicM1M2 :: V 2 Rational -> Int -> Q3S.QED3Single
q3SFeasibleBasicM1M2 mDims nmax = Q3S.QED3Single
  { externalDims = Q3S.ExternalDims {..}
  , spectrum     = setGaps
    [ (m2Channel, deltaM2)
    ] $ setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3S.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaM1, deltaM2) = fromV mDims
    m2Channel = mkChannel (4,0) (4,0) 0

q3SFeasibleDefaultM1M2 :: V 2 Rational -> Int -> Q3S.QED3Single
q3SFeasibleDefaultM1M2 mDims nmax = Q3S.QED3Single
  { externalDims = Q3S.ExternalDims {..}
  , spectrum     = setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0) 0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0) 0, 2.8)
    -- q=0 even
    , (mkChannel (4,0) (0,0) 0, 5.5)
    , (mkChannel (3,1) (0,0) 1, 4)
    , (mkChannel (2,2) (0,0) 0, 2.8)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- M2 gaps
    , (mkChannel (0,0) (4,0) 0, deltaM2)
    , (mkChannel (2,0) (4,0) 0, deltaM2)
    , (mkChannel (1,1) (4,0) 1, deltaM2)
    , (mkChannel (4,0) (4,0) 0, deltaM2)
    , (mkChannel (3,1) (4,0) 1, deltaM2)
    , (mkChannel (2,2) (4,0) 0, deltaM2)
    ] $ setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3S.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaM1, deltaM2) = fromV mDims

q3SFeasibleDefaultM1M2SSb :: V 3 Rational -> Int -> Q3S.QED3Single
q3SFeasibleDefaultM1M2SSb dims nmax = Q3S.QED3Single
  { externalDims = Q3S.ExternalDims {..}
  , spectrum     = setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0) 0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0) 0, 2.8)
    -- q=0 even
    , (mkChannel (4,0) (0,0) 0, 5.5)
    , (mkChannel (3,1) (0,0) 1, 4)
    , (mkChannel (2,2) (0,0) 0, deltaSSb)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- M2 gaps
    , (mkChannel (0,0) (4,0) 0, deltaM2)
    , (mkChannel (2,0) (4,0) 0, deltaM2)
    , (mkChannel (1,1) (4,0) 1, deltaM2)
    , (mkChannel (4,0) (4,0) 0, deltaM2)
    , (mkChannel (3,1) (4,0) 1, deltaM2)
    , (mkChannel (2,2) (4,0) 0, deltaM2)
    ] $ setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3S.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaM1, deltaM2, deltaSSb) = fromV dims

-- | FOR DELAUNAY SEARCH
qed3DimVector :: Bound prec Q3S.QED3Single -> V 2 Rational
qed3DimVector Bound{ boundKey = i } =
  toV (dm1, dm2)
  where
    dm1 = Q3S.deltaM1 (Q3S.externalDims i)
    Blocks.Fixed dm2 = Map.findWithDefault (Blocks.Fixed 0) (mkChannel (4,0) (4,0) 0)
                       (deltaGaps (Q3S.spectrum i))

qed3DimVector3 :: Bound prec Q3S.QED3Single -> V 3 Rational
qed3DimVector3 Bound{ boundKey = i } =
  toV (dm1, dm2, ssb)
  where
    dm1 = Q3S.deltaM1 (Q3S.externalDims i)
    Blocks.Fixed dm2 = Map.findWithDefault (Blocks.Fixed 0) (mkChannel (4,0) (4,0) 0)
                       (deltaGaps (Q3S.spectrum i))
    Blocks.Fixed ssb = Map.findWithDefault (Blocks.Fixed 0) (mkChannel (2,2) (0,0) 0)
                       (deltaGaps (Q3S.spectrum i))

newQED3CheckpointMap
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3S.QED3Single) FilePath)
newQED3CheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")

newQED3CheckpointMap3
 :: AffineTransform 3 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3S.QED3Single) FilePath)
newQED3CheckpointMap3 affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector3 mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

remoteComputeWithCheckpoints
 :: TrackedMap Cluster (Bound Int Q3S.QED3Single) FilePath
 -> Bound Int Q3S.QED3Single
 -> Cluster Bool
remoteComputeWithCheckpoints checkpointMap bound = do
  jobTime <- asks (Slurm.time . clusterJobOptions)
  -- create working directory and files with initial/final checkpoint
  workDir <- newWorkDir bound
  initCheckpoint <- OPE.get checkpointMap bound
  let boundFiles = (Bound.defaultBoundFiles workDir){ initialCheckpointDir = initCheckpoint }
      thisCheckpoint = checkpointDir boundFiles
  -- submit job and get SDPB result
  result <- remoteEvalJob $
            static compute `ptrAp` cPure jobTime `cAp` cPure bound `cAp` cPure boundFiles
  when (SDPB.isFinished result) $
    OPE.set checkpointMap bound thisCheckpoint
  return (SDPB.isPrimalFeasible result)
  where
    compute jobTime bound' boundFiles' = do
      -- allow for cleanup time
      solverParams' <- liftIO $ Bound.setFixedTimeLimit (0.9*jobTime) (solverParams bound')
      Bound.computeClean' (bound' { solverParams = solverParams' }) boundFiles'

      --liftIO $ mapM_ removePathForcibly [outDir boundFiles]

-- | START OF BOUNDS PROGRAMS --------------------------------------------------
boundsProgram :: Text -> Cluster ()

boundsProgram "QED3Single_test_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (2*hour) . setJobMemory "90G") $ void $
  Bound.remoteCompute $ bound (toV (2.7, 9.0))
  where
    nmax = 6
    bound deltaExts = Bound
      { --boundKey = (q3SFeasibleBasicM1M2 deltaExts nmax)
        boundKey = (q3SFeasibleDefaultM1M2 deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "QED3Single_grid_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (12*hour) . setJobMemory "100G") $ void $
  mapConcurrently_ (remoteComputeQ3SBoundMulti . map bound) $ chunksOf 5 pointList
  where
    nmax = 10
    bound deltaExts = Bound
      { --boundKey = (q3SFeasibleBasicM1M2 deltaExts nmax)
        boundKey = (q3SFeasibleDefaultM1M2 deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    pointList = [ (toV (x,y)) | x <- [2.4, 2.5 .. 3.0 ]
                              , y <- [5.0, 6.0 .. 12.0] ]

boundsProgram "QED3Single_gridT4_nmax6" =
  local (setJobType (MPIJob 1 18) . setJobTime (1*hour)  . setSlurmPartition "scavenge") $ void $
  mapConcurrently_ Bound.remoteCompute (map bound pointList)
  where
    nmax = 6
    bound deltaExts = Bound
      { boundKey = (q3SFeasibleBasicM1M2 deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    --pointList = [ (toV (x,y)) | x <- [0.5] --[0.9, 1.0 .. 1.2 ]
    --                          , y <- [4.5, 5.0 .. 12 ] ]
    pointList = toV <$> [(0.502, 1.0011), (0.5007, 1.0472), (1.1436, 3.2425), (1.1423, 3.4324), (0.7152, 1.7012), (0.7152, 1.864), (0.9297, 2.6157), (0.9304, 2.4474)]

boundsProgram "QED3Single_default_delaunay_nmax6" =
  local (setJobType (MPIJob 1 18)  . setJobTime (1*hour) . setSlurmPartition "pi_poland,scavenge") $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (remoteComputeWithCheckpoints checkpointMap . bound)
  where
    nmax = 6
    affine = AffineTransform
      { affineShift  = toV (2.7, 8.5)
      , affineLinear = toV ( toV (0.3, 0)
                           , toV (0, 4.0)
                           )
      }
    bound deltaExts = Bound
      { boundKey = (q3SFeasibleDefaultM1M2 deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initPts = Map.fromList $
      [(p, Just True)  | p <- initAllowed] ++
      [(p, Just False) | p <- initDisallowed]
      where
        initAllowed    = toV <$> [(2.8 ,  6.0)]
        initDisallowed = toV <$> [(2.41, 12.0)]

boundsProgram "QED3Single_T4_delaunay_nmax6" =
  local (setJobType (MPIJob 1 28)  . setJobTime (1*hour) . setSlurmPartition "pi_poland,scavenge" ) $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  void $ delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initPts
    (remoteComputeWithCheckpoints checkpointMap . bound)
  where
    nmax = 6
    affine = AffineTransform
      { affineShift  = toV (0.85, 2.25)
      , affineLinear = toV ( toV (0.45, 0)
                           , toV (0, 1.75)
                           )
      }
    bound deltaExts = Bound
      { boundKey = (q3SFeasibleBasicM1M2 deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initPts = Map.fromList $
      [(p, Just True)  | p <- initAllowed] ++
      [(p, Just False) | p <- initDisallowed]
      where
        initAllowed    = toV <$> [(0.502 , 1.0011), (1.1436, 3.2425), (0.7152, 1.7012), (0.9304, 2.4474)]
        initDisallowed = toV <$> [(0.5007, 1.0472), (1.1423, 3.4324), (0.7152, 1.864 ), (0.9297, 2.6157)]

boundsProgram "QED3Single_SSb_delaunay2d_nmax10" = 
  local (setJobType (MPIJob 1 18)  . setJobTime (5*hour)
         . setSlurmPartition "pi_poland,scavenge" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (remoteComputeWithCheckpoints checkpointMap . bound)
  where
    nmax = 10
    ssb  = 10
    affine = AffineTransform
      { affineShift  = toV (2.25, 7.5)
      , affineLinear = toV ( toV (0.75, 0)
                           , toV (0   , 5)
                           )
      }
    bound deltaExts = Bound
      { boundKey = q3SFeasibleDefaultM1M2SSb (L.snoc deltaExts ssb) nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 200
    initPts = Map.fromList $ getSSB2d ssb

boundsProgram "QED3Single_SSb_delaunay3d_nmax10" =
--  local (setJobType (MPIJob 1 18)  . setJobTime (1*hour) . setSlurmPartition "pi_poland,scavenge") $ do
  local (setJobType (MPIJob 1 28)  . setJobTime (3*hour)
         . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap3 affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (remoteComputeWithCheckpoints checkpointMap . bound)
  where
    nmax = 10
    affine = AffineTransform
      { affineShift  = toV (2.7, 8.5, 6)
      , affineLinear = toV ( toV (0.3, 0, 0)
                           , toV (0  , 4, 0)
                           , toV (0  , 0, 4)
                           )
      }
    bound deltaExts = Bound
      { boundKey = (q3SFeasibleDefaultM1M2SSb deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 300
    initPts = Map.fromList []

boundsProgram "QED3Single_dumpBulkConstraints" =
  local (setJobType (MPIJob 1 18)  . setJobTime (1*hour) ) $ do
  mapM_ printConstraint $ Q3S.bulkConstraints $ (q3SFeasibleBasicM1M2 (toV (1.1, 2.55)) 6)
  where
    printConstraint :: (MonadIO m) => Q3S.BulkConstraint -> m ()
    printConstraint (Q3S.BulkConstraint dRange (Q3S.GeneralChannel cRep fRep)) =
      Log.text $ Log.showText (cRep, fRep, dRange)
    printConstraint _ = pure ()

boundsProgram p = unknownProgram p

matrixDumps :: (String, [String])
matrixDumps = Q3S.qed3ShowSDP (q3SFeasibleBasicM1M2 (toV (1.1, 2.55)) 6)
