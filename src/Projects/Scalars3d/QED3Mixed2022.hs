{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeApplications      #-}

module Projects.Scalars3d.QED3Mixed2022 where

import qualified Blocks                             as Blocks
import qualified Blocks.ScalarBlocks                as SB
import           Bootstrap.Bounds.BoundDirection         (BoundDirection (..))
import           Bootstrap.Bounds.Spectrum          (addIsolated, deltaGap, deltaGaps,
                                                     deltaIsolated, setGap, setGaps, setTwistGap,
                                                     unitarySpectrum)
import           Bootstrap.Math.Linear              (fromRows, fromV, toV)
import qualified Bootstrap.Math.Linear              as L
--import           Bootstrap.Math.VectorSpace         (zero, (*^))
import qualified Bootstrap.Math.VectorSpace         as VS
import           Bounds.Scalars3d.QED3Rep           (ONRep (..), fromFirstTwoTuple, toRepName)
import qualified Bounds.Scalars3d.QED3Mixed         as Q3M
import           Control.Monad                      (forM_, mapM_, void, when)
import           Control.Monad.IO.Class             (MonadIO, liftIO)
import           Control.Monad.Reader               (asks, local)
import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.List                          (intercalate)
import           Data.List.Split                    (chunksOf)
import qualified Data.Map.Strict                    as Map
import           Data.Maybe                         (fromMaybe, listToMaybe)
import qualified Data.Set                           as Set
import           Data.Text                          (Text, pack, splitOn, strip,
                                                     unpack)
import qualified Data.Text
import           Data.Time.Clock                    (NominalDiffTime)
import           Hyperion
import           Bootstrap.Math.AffineTransform     (AffineTransform (..), apply)
import qualified Hyperion.Bootstrap.BinarySearch    as BS
import           Hyperion.Bootstrap.Bound           (Bound (..),
                                                     BoundFiles (..))
import qualified Hyperion.Bootstrap.Bound           as Bound
import           Hyperion.Bootstrap.DelaunaySearch  (DelaunayConfig (..),
                                                     delaunaySearchRegionPersistent)
import           Hyperion.Bootstrap.Main            (unknownProgram)
import           Hyperion.Bootstrap.OPESearch       (BilinearForms (..),
                                                     OPESearchConfig (..),
                                                     TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch       as OPE
import qualified Hyperion.Bootstrap.OPESearch.TrackedMap as TM
import qualified Hyperion.Bootstrap.Params          as Params
import qualified Hyperion.Bootstrap.SDPDeriv        as SDPDeriv
import qualified Hyperion.Bootstrap.SDPDeriv.BFGS   as BFGS
import qualified Hyperion.Bootstrap.SDPDeriv.Newton as Newton
import qualified Hyperion.Database                  as DB
import qualified Hyperion.Log                       as Log
import qualified Hyperion.Slurm                     as Slurm
import           Hyperion.Util                      (hour, minute)
import           Linear.V                           (V)
import           Linear.Vector                      (zero)
import           Numeric                            (readFloat, readSigned)
import           Numeric.Rounded                    (Rounded, RoundingMode (..))
import           Projects.Scalars3d.Defaults        (defaultBoundConfig,
                                                     defaultDelaunayConfig,
                                                     defaultQuadraticNetConfig)
import           Projects.Scalars3d.Delaunay2       (delaunaySearchCubePersistent)
import           SDPB                               (Params (..))
import qualified SDPB
import           System.Directory                   (removePathForcibly)
import           System.FilePath.Posix              ((</>))
import           Data.Typeable                      (Typeable)
import           Projects.Scalars3d.QED3MixedData  (aabPoints, aabPoints3d)

qed3DimVector2 :: Bound prec Q3M.QED3Mixed -> V 2 Rational
qed3DimVector2 Bound{ boundKey = i } = toV
  ( Q3M.deltaMHalf (Q3M.externalDims i)
  , Q3M.deltaM1    (Q3M.externalDims i) )

mkChannel :: (Int, Int) -> (Int, Int) -> Int -> Q3M.ChannelType
mkChannel o6rep o2rep spin =
  Q3M.ChannelType spin ( fromFirstTwoTuple o6rep, fromFirstTwoTuple o2rep )

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

checkBoundOutput :: Cluster (SDPB.Output) -> Cluster Bool
checkBoundOutput = fmap ((==SDPB.PrimalFeasibleJumpDetected) . SDPB.terminateReason)

newQED3CheckpointMap
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3M.QED3Mixed) FilePath)
newQED3CheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector2 mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")

newQED3CheckpointMapS :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3M.QED3Mixed) FilePath)
newQED3CheckpointMapS affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector2s mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")
 where
   singlet = mkChannel (0,0) (0,0) 0
   qed3DimVector2s Bound{ boundKey = i } = toV
     ( case head $ Set.elems $ deltaIsolated (Q3M.spectrum i) singlet of
         Blocks.Fixed r -> r
         Blocks.RelativeUnitarity _ -> undefined
     , case deltaGap (Q3M.spectrum i) singlet of
         Blocks.Fixed r -> r
         Blocks.RelativeUnitarity _ -> undefined
     )

newQED3CheckpointMapA
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3M.QED3Mixed) FilePath)
newQED3CheckpointMapA affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector2s mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")
 where
   aab = mkChannel (2,0) (0,0) 0
   qed3DimVector2s Bound{ boundKey = i } = toV
     ( case head $ Set.elems $ deltaIsolated (Q3M.spectrum i) aab of
         Blocks.Fixed r -> r
         Blocks.RelativeUnitarity _ -> undefined
     , case deltaGap (Q3M.spectrum i) aab of
         Blocks.Fixed r -> r
         Blocks.RelativeUnitarity _ -> undefined
     )

newQED3CheckpointMap3
 :: AffineTransform 3 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3M.QED3Mixed) FilePath)
newQED3CheckpointMap3 affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector3 mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")
 where
   mHalfPrime = mkChannel (1,0) (1,0) 0
   qed3DimVector3 :: Bound prec Q3M.QED3Mixed -> V 3 Rational
   qed3DimVector3 Bound{ boundKey = i } = toV
     ( Q3M.deltaMHalf (Q3M.externalDims i)
     , Q3M.deltaM1    (Q3M.externalDims i)
     , case deltaGap (Q3M.spectrum i) mHalfPrime of
         Blocks.Fixed r -> r
         Blocks.RelativeUnitarity _ -> undefined
     )

newQED3CheckpointMap3Aab
 :: AffineTransform 3 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (Bound Int Q3M.QED3Mixed) FilePath)
newQED3CheckpointMap3Aab affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine qed3DimVector3 mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "qed3Checkpoints")
 where
   aabRep = mkChannel (2,0) (0,0) 0
   qed3DimVector3 :: Bound prec Q3M.QED3Mixed -> V 3 Rational
   qed3DimVector3 Bound{ boundKey = i } = toV
     ( Q3M.deltaMHalf (Q3M.externalDims i)
     , Q3M.deltaM1    (Q3M.externalDims i)
     , case head $ Set.elems $ deltaIsolated (Q3M.spectrum i) aabRep of
         Blocks.Fixed x -> x
         Blocks.RelativeUnitarity _ -> undefined
     )

q3mFeasibleDefaultGaps :: V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleDefaultGaps mDims nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=3/2
    , (mkChannel (1,0) (3,0) 0, 3)
    , (mkChannel (3,0) (3,0) 0, 3)
    , (mkChannel (2,1) (3,0) 0, 3)
    , (mkChannel (1,0) (3,0) 1, 3)
    , (mkChannel (3,0) (3,0) 1, 3)
    , (mkChannel (2,1) (3,0) 1, 3)
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    -- special for MhM1 mixed (from Yuan)
    , (mkChannel (1,0) (1,0)  0, max deltaMHalf 3)
    , (mkChannel (3,0) (1,0)  0, deltaMHalf)
    , (mkChannel (2,1) (1,0)  0, deltaMHalf)
    , (mkChannel (2,0) (2,0) 0, max deltaM1 5)
    , (mkChannel (0,0) (2,0) 0, deltaM1)
    , (mkChannel (1,1) (2,0) 1, deltaM1)
    ] $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaMHalf, deltaM1) = fromV mDims

q3mFeasibleSingletGaps :: Rational -> Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleSingletGaps sIso sspGap mDims nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, sspGap)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=3/2
    , (mkChannel (1,0) (3,0) 0, 3)
    , (mkChannel (3,0) (3,0) 0, 3)
    , (mkChannel (2,1) (3,0) 0, 3)
    , (mkChannel (1,0) (3,0) 1, 3)
    , (mkChannel (3,0) (3,0) 1, 3)
    , (mkChannel (2,1) (3,0) 1, 3)
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    -- special for MhM1 mixed (from Yuan)
    , (mkChannel (1,0) (1,0)  0, max deltaMHalf 3)
    , (mkChannel (3,0) (1,0)  0, deltaMHalf)
    , (mkChannel (2,1) (1,0)  0, deltaMHalf)
    , (mkChannel (2,0) (2,0) 0, max deltaM1 5)
    , (mkChannel (0,0) (2,0) 0, deltaM1)
    , (mkChannel (1,1) (2,0) 1, deltaM1)
    ] $
    addIsolated (mkChannel (0,0) (0,0) 0) sIso $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaMHalf, deltaM1) = fromV mDims

q3mFeasibleT3VGaps :: Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleT3VGaps t3v mDims nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=3/2
    , (mkChannel (1,0) (3,0) 0, 3)
    , (mkChannel (3,0) (3,0) 0, 3)
    , (mkChannel (2,1) (3,0) 0, 3)
    , (mkChannel (1,0) (3,0) 1, 3)
    , (mkChannel (3,0) (3,0) 1, 3)
    , (mkChannel (2,1) (3,0) 1, 3)
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    -- special for MhM1 mixed (from Yuan)
    , (mkChannel (1,0) (1,0)  0, max deltaMHalf 3)
    , (mkChannel (3,0) (1,0)  0, t3v)
    , (mkChannel (2,1) (1,0)  0, deltaMHalf)
    , (mkChannel (2,0) (2,0) 0, max deltaM1 5)
    , (mkChannel (0,0) (2,0) 0, deltaM1)
    , (mkChannel (1,1) (2,0) 1, deltaM1)
    ] $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaMHalf, deltaM1) = fromV mDims

q3mFeasibleSingletGaps' :: V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleSingletGaps' sDims nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, sspGap)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=3/2
    , (mkChannel (1,0) (3,0) 0, 3)
    , (mkChannel (3,0) (3,0) 0, 3)
    , (mkChannel (2,1) (3,0) 0, 3)
    , (mkChannel (1,0) (3,0) 1, 3)
    , (mkChannel (3,0) (3,0) 1, 3)
    , (mkChannel (2,1) (3,0) 1, 3)
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    -- special for MhM1 mixed (from Yuan)
    , (mkChannel (1,0) (1,0)  0, max deltaMHalf 3)
    , (mkChannel (3,0) (1,0)  0, deltaMHalf)
    , (mkChannel (2,1) (1,0)  0, deltaMHalf)
    , (mkChannel (2,0) (2,0) 0, max deltaM1 5)
    , (mkChannel (0,0) (2,0) 0, deltaM1)
    , (mkChannel (1,1) (2,0) 1, deltaM1)
    ] $
    addIsolated (mkChannel (0,0) (0,0) 0) sIso $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    deltaMHalf = 1.022
    deltaM1    = 2.499
    (sIso, sspGap) = fromV sDims

q3mFeasibleDefaultGaps3d :: V 3 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleDefaultGaps3d mDims nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=3/2
    , (mkChannel (1,0) (3,0) 0, 3) -- Maybe try these?
    , (mkChannel (3,0) (3,0) 0, 3)
    , (mkChannel (2,1) (3,0) 0, 3)
    , (mkChannel (1,0) (3,0) 1, deltaMHalfP) --3 --Maybe try these?!!! (various values, 3.5ish?)
    , (mkChannel (3,0) (3,0) 1, deltaMHalfP) --3
    , (mkChannel (2,1) (3,0) 1, deltaMHalfP) --3
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    -- 
    -- , (mkChannel (1,0) (1,0) 1, 3)
    -- special for MhM1 mixed (from Yuan)
    , (mkChannel (1,0) (1,0)  0, max deltaMHalf 3) --deltaMHalfP
    , (mkChannel (3,0) (1,0)  0, deltaMHalf) --deltaMHalfP
    , (mkChannel (2,1) (1,0)  0, deltaMHalf) --deltaMHalfP
    , (mkChannel (2,0) (2,0) 0, max deltaM1 5)
    , (mkChannel (0,0) (2,0) 0, deltaM1)
    , (mkChannel (1,1) (2,0) 1, deltaM1)
    ] $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    (deltaMHalf, deltaM1, deltaMHalfP) = fromV mDims

q3mFeasibleCutM1Gaps :: Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleCutM1Gaps deltaM1 nmax = Q3M.QED3Mixed
  { externalDims = Q3M.ExternalDims {..}
  , spectrum     =  setGaps
    -- singlet-singlet
    [ (mkChannel (0,0) (0,0)  0, 3)
    -- AAb
    , (mkChannel (2,0) (0,0)  0, 2.8)
    -- SSb
    , (mkChannel (2,2) (0,0)  0, 3)
    -- q=0 even
    , (mkChannel (4,0) (0,0)  0, 5.5)
    , (mkChannel (3,1) (0,0)  1, 4)
    -- q=0 odd
    , (mkChannel (4,0) (1,1) 1, 7)
    , (mkChannel (3,1) (1,1) 0, 4)
    , (mkChannel (2,2) (1,1) 1, 4)
    -- q=2
    , (mkChannel (0,0) (4,0) 0, 5)
    , (mkChannel (2,0) (4,0) 0, 5)
    , (mkChannel (1,1) (4,0) 1, 5)
    , (mkChannel (4,0) (4,0) 0, 5)
    , (mkChannel (3,1) (4,0) 1, 5)
    , (mkChannel (2,2) (4,0) 0, 5)
    ] $
    setTwistGap 1e-6 $ unitarySpectrum
  , objective    = Q3M.Feasibility
  , spins        = Params.spinsNmax nmax
  , blockParams  = Params.blockParamsNmax nmax
  }
  where
    deltaMHalf = 1.25

q3mFeasibleAAbGaps :: Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleAAbGaps aab mDims nmax =
  q0 { Q3M.spectrum = setGap (mkChannel (2,0) (0,0) 0) aab $ Q3M.spectrum q0 }
  where
    q0 = q3mFeasibleDefaultGaps mDims nmax

q3mFeasibleAAbIsoGaps :: Rational -> Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleAAbIsoGaps aab aabP mDims nmax =
  q0 { Q3M.spectrum = addIsolated aabRep aab $ setGap aabRep aabP $ Q3M.spectrum q0 }
  where
    q0 = q3mFeasibleDefaultGaps mDims nmax
    aabRep = mkChannel (2,0) (0,0) 0

q3mFeasibleAAbIsoGaps' :: V 2 Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleAAbIsoGaps' mDims aabs nmax =
  q0 { Q3M.spectrum = addIsolated aabRep iso $ setGap aabRep gap $ Q3M.spectrum q0 }
  where
    q0 = q3mFeasibleDefaultGaps mDims nmax
    aabRep = mkChannel (2,0) (0,0) 0
    (iso, gap) = fromV aabs

q3mFeasibleAAb3d :: Rational -> V 3 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleAAb3d aabP searchDims nmax =
  q0 { Q3M.spectrum = addIsolated aabRep deltaAAb $ setGap aabRep aabP $ Q3M.spectrum q0 }
  where
    q0 = q3mFeasibleDefaultGaps (toV (deltaMHalf, deltaM1)) nmax
    aabRep = mkChannel (2,0) (0,0) 0
    (deltaMHalf, deltaM1, deltaAAb) = fromV searchDims

q3mFeasibleAAb3d' :: Rational -> V 3 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleAAb3d' aabP searchDims nmax =
  q0 { Q3M.spectrum =
         addIsolated singletRep 3.75 $ setGap singletRep 8 $
         addIsolated aabRep deltaAAb $ setGap aabRep aabP $
         Q3M.spectrum q0
     }
  where
    q0 = q3mFeasibleDefaultGaps (toV (deltaMHalf, deltaM1)) nmax
    aabRep = mkChannel (2,0) (0,0) 0
    singletRep = mkChannel (0,0) (0,0) 0
    (deltaMHalf, deltaM1, deltaAAb) = fromV searchDims

q3mFeasibleHSGaps :: Rational -> V 2 Rational -> Int -> Q3M.QED3Mixed
q3mFeasibleHSGaps hs mDims nmax =
  q0 { Q3M.spectrum = setGap (mkChannel (3,1) (0,0) 1) hs $ Q3M.spectrum q0 }
  where
    q0 = q3mFeasibleDefaultGaps mDims nmax

q3mMinCJt :: V 2 Rational -> Int -> Q3M.QED3Mixed
q3mMinCJt mDims nmax =
  -- minimizing C*Jt is equivalent to maximizing the Jt OPE coeff
  (q3mFeasibleDefaultGaps mDims nmax) { Q3M.objective = Q3M.JtOPEBound UpperBound }

-- probably wrong:
q3mMaxCJt :: V 2 Rational -> Int -> Q3M.QED3Mixed
q3mMaxCJt mDims nmax =
  (q3mFeasibleDefaultGaps mDims nmax) { Q3M.objective = Q3M.JtOPEBound LowerBound }

remoteComputeWithSpectrum :: Int -> (Int -> Q3M.QED3Mixed) -> Cluster SDPB.Output
remoteComputeWithSpectrum nmax boundFromNmax = do
  wkDir <- newWorkDir bound
  -- Log.info "precision" $ SDPB.precision (solverParams bound)
  Log.info "point workdir" ((fromRational @Double) <$> qed3DimVector2 bound, wkDir)
  let sdpParts = Bound.withSDPDeps bound SDPB.parts
      conFiles = SDPB.partPath "" <$> drop 2 sdpParts
      bulkCons = Q3M.bulkConstraints (boundKey bound)
      -- see Bounds file for constraint ordering
      constraintStrs = zipWith ($)
        (map prettyPrintCh bulkCons  ++ map prettyPrintNamedCh ["stress", "flavor", "external"])
        conFiles
  -- dump channels
  liftIO $ writeFile (wkDir </> "spectrumKey.json") ("[" ++ intercalate ",\n" constraintStrs ++ "]\n")
  -- run
  Bound.remoteComputeWithFileTreatment Bound.keepAllFiles Bound.defaultBoundFiles bound
  where
    bound = Bound
      { boundKey = boundFromNmax nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax)
        { precision     = 768
        , writeSolution = Set.fromList [SDPB.PrimalVector_x, SDPB.DualVector_y]
        }
      , boundConfig = defaultBoundConfig
      }
    prettyPrintCh (Q3M.BulkConstraint _ (Q3M.GeneralChannel cRep (r6, r2))) fname =
      "{" ++ 
      withTag "filename" fname ++ ", " ++
      withTag "rep" ("rep[" ++ toRepName r6 ++ "," ++ toRepName r2 ++ "]") ++ ", " ++
      withTag "l" (SB.symTensorSpin cRep) ++ ", " ++
      (case SB.symTensorDelta cRep of
         (Blocks.Fixed dim) -> withTag "bound" (fromRational dim)
         _ -> withTag "bound" "unitarity"
      ) ++ "}"
    prettyPrintNamedCh chName fname =
      "{" ++ 
      withTag "filename" fname ++ ", " ++
      withTag "namedChannel" chName ++ "}"
    withTag :: (Show a) => String -> a -> String
    withTag tagName value = show tagName ++ ": " ++ show value

-- | START OF BOUNDS PROGRAMS --------------------------------------------------
boundsProgram :: Text -> Cluster ()

boundsProgram "QED3Mixed_test_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (2*hour) . setJobMemory "90G") $ void $
  Bound.remoteCompute $ bound (toV (1.1, 2.6))
  where
    nmax = 6
    bound deltaExts = Bound
      { boundKey = (q3mFeasibleDefaultGaps deltaExts nmax)
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      -- SWAP FOR defaultParams
      , boundConfig = defaultBoundConfig
      }
  
boundsProgram "QED3Mixed_getSpectrum_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (4*hour) . setJobMemory "90G") $ void $
  forConcurrently_ [(1.022 + x, 2.499 + 4*x) | x <- [0, 0.01 .. 0.1]]
  (\pt -> remoteComputeWithSpectrum 6 (q3mMinCJt $ toV pt))

boundsProgram "QED3Mixed_getSpectrum2_nmax6" =
  local (setJobType (MPIJob 1 28) . setJobTime (4*hour) . setJobMemory "90G") $ void $
  forConcurrently_ [(1.022 + x, 2.499 + 4*x) | x <- [0, 0.01 .. 0.1]]
  (\pt -> remoteComputeWithSpectrum 6 (q3mMaxCJt $ toV pt))

boundsProgram "QED3Mixed_getSpectrum_nmax10" =
  local (setJobType (MPIJob 1 28) . setJobTime (24*hour) . setJobMemory "90G") $ void $
  forConcurrently_ [(1.022 + x, 2.499 + 4*x) | x <- [0, 0.02 .. 0.1]]
  (\pt -> remoteComputeWithSpectrum 10 (q3mMinCJt $ toV pt))

boundsProgram "QED3Mixed_default_delaunay_nmax6" =
  local (setJobType (MPIJob 1 18)  . setJobTime (2*hour) . setSlurmPartition "pi_poland") $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  -- lambdaMap     <- newQED3LambdaMap affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
    -- (remoteOPESearchQED3 checkpointMap lambdaMap 0.4 1.4 . bound)
  where
    nmax = 6
    affine = AffineTransform
      { affineShift  = toV (1.2, 2.5)
      , affineLinear = toV ( toV (0.3, 0  )
                           , toV (0   , 0.3)
                           )
      }
    bound deltaExts = Bound
      { boundKey =  q3mFeasibleDefaultGaps deltaExts nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    initPts = Map.fromList $
      [(toV p, Just True)  | p <- initAllowed] ++
      [(toV p, Just False) | p <- initDisallowed]
      where
        initAllowed    = []
        initDisallowed = []

boundsProgram "QED3Mixed_default_delaunay_nmax10" =
  local (setJobType (MPIJob 1 28)  . setJobTime (16*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  -- lambdaMap     <- newQED3LambdaMap affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
    -- (remoteOPESearchQED3 checkpointMap lambdaMap 0.4 1.4 . bound)
  where
    nmax = 10
    affine = AffineTransform
      { affineShift  = toV (1.2, 2.45)
      , affineLinear = toV ( toV (0.3, 0  )
                           , toV (0   , 0.25)
                           )
      }
    bound deltaExts = Bound
      { boundKey =  q3mFeasibleDefaultGaps deltaExts nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 100
    initPts = Map.fromList $
      [(toV p, Just True)  | p <- initAllowed] ++
      [(toV p, Just False) | p <- initDisallowed]
      where
        initAllowed    = []
        initDisallowed = []

-- boundsProgram "QED3Mixed_singlet_delaunay_nmax10" =
-- boundsProgram "QED3Mixed_t3v_delaunay_nmax10" =
-- boundsProgram "QED3Mixed_aab_delaunay_nmax10" =
-- boundsProgram "QED3Mixed_aabIso_delaunay_nmax10" =
boundsProgram "QED3Mixed_aabIso_delaunay_nmax10" =
-- boundsProgram "QED3Mixed_HS_delaunay_nmax10" =
  local (setJobType (MPIJob 1 28)  . setJobTime (12*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap affine (Just defaultCheckpoint)
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 10
    -- defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-04/EwRGv/Object_QzvJ_FXS8BEy0BHKYNnbWx5lpPb_J1NUZD6GsHBnJdY/ck"
    -- for t3v (or anything with no isolation):
    -- defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-04/kUIRd/Object_Y8JoJ1VNHD494xANfh-ZIjIKYp9lv1HHwZNNdC5MVuM/ck"
    -- for aab iso
    -- defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-05/zTUQq/Object_HriaZwF-6Eg7j4Uk62TO5veWyRrXEn98PlTLc6lMPFA/ck"
    -- for aab iso 6:
    defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-05/OHpyQ/Object_3WxYu2mPaEHKuTR3Nh5fFtLIFqc51-Bo91yRNaAUUf4/ck"
    affine = AffineTransform
      { affineShift  = toV (1.2, 2.45 + 0.02)
      , affineLinear = toV ( toV (0.3, 0  )
                           , toV (0   , 0.25 + 0.02)
                           )
      }
    bound deltaExts = Bound
      { boundKey =  --q3mFeasibleSingletGaps 3.75 8.5 deltaExts nmax
          --q3mFeasibleT3VGaps 5 deltaExts nmax -- submitted: 3, 4, 5
          --q3mFeasibleAAbGaps 4 deltaExts nmax -- submitted: 3, 4
          -- (x,6) submitted: 2.75, 2.85
          q3mFeasibleAAbIsoGaps 2.85 6 deltaExts nmax -- submitted: 5, 7, 6
          -- q3mFeasibleHSGaps 7 deltaExts nmax -- submitted: 5, 7
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 200
    initPts = Map.fromList $
      [ (toV p, Just True)  | p <- initAllowed    ] ++
      [ (toV p, Just False) | p <- initDisallowed ] ++
      [ (toV p, Nothing)    | p <- initSearch     ]
      where
        initAllowed = []
        initDisallowed = []
        initSearch = [ (1.022, 2.499) ]
        -- initAllowed    = [ (x,y) | (x,y,isAllowed) <- aabPoints,     isAllowed ]
        -- initDisallowed = [ (x,y) | (x,y,isAllowed) <- aabPoints, not isAllowed ]
        -- initSearch     = [ (x,y) | x <- [1.02, 1.04 .. 1.08]
        --                          , y <- [2.66, 2.68 .. 2.74] ]

boundsProgram "QED3Mixed_aab_isogap_delaunay_nmax10" =
  local (setJobType (MPIJob 1 28)  . setJobTime (12*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMapA affine (Just defaultCheckpoint)
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 10
    --defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-04/EwRGv/Object_QzvJ_FXS8BEy0BHKYNnbWx5lpPb_J1NUZD6GsHBnJdY/ck"
    --defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-04/vHKVx/Object_8gNRyirVG3GsB5deAsbtNto1-FamOg-8oYc_306XDKk/ck"
    --for aab iso 6:
    defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-05/OHpyQ/Object_3WxYu2mPaEHKuTR3Nh5fFtLIFqc51-Bo91yRNaAUUf4/ck"
    affine = AffineTransform
      { affineShift  = toV (2.8, 6)
      , affineLinear = toV ( toV (0.4, 0  )
                           , toV (0   , 1  )
                           )
      }
    bound aab = Bound
      { boundKey = q3mFeasibleAAbIsoGaps' largeNPoint aab nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 10 150
    largeNPoint = toV (1.022, 2.499)
    initPts = Map.fromList $
      [ (toV p, Just True)  | p <- initAllowed    ] ++
      [ (toV p, Just False) | p <- initDisallowed ] ++
      [ (toV p, Nothing)    | p <- initSearch     ]
      where
        initAllowed = []
        initDisallowed = []
        initSearch     = []

boundsProgram "QED3Mixed_delaunay3d_nmax10" =
  local (setJobType (MPIJob 1 28)  . setJobTime (12*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap3 affine Nothing
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 10
    affine = AffineTransform
      { affineShift  = toV (1.2, 2.45, 6)
      , affineLinear = toV ( toV (0.3, 0   , 0)
                           , toV (0  , 0.25, 0)
                           , toV (0  , 0   , 3)
                           )
      }
    bound point = Bound
      { boundKey =  q3mFeasibleDefaultGaps3d point nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 12 300
    initPts = Map.fromList $
      [(toV p, Just True)  | p <- initAllowed] ++
      [(toV p, Just False) | p <- initDisallowed]
      where
        initAllowed    = []
        initDisallowed = []

boundsProgram "QED3Mixed_delaunay3d_aab_nmax10" =
  local (setJobType (MPIJob 1 28)  . setJobTime (12*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  checkpointMap <- newQED3CheckpointMap3Aab affine Nothing -- (Just defaultCheckpoint)
  void $ delaunaySearchCubePersistent delaunaySearchPoints delaunayConfig affine initPts
    (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  where
    nmax = 10
    affine = AffineTransform
      { affineShift  = toV (1.2, 2.45, 3.0)
      , affineLinear = toV ( toV (0.3, 0   , 0   )
                           , toV (0  , 0.25, 0   )
                           , toV (0  , 0   , 0.35)
                           )
      }
    bound point = Bound
      { boundKey =  q3mFeasibleAAb3d' 6 point nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    --for aab iso 6:
    -- defaultCheckpoint = "/home/msm233/palmer_scratch/hyperion-data/data/2023-05/OHpyQ/Object_3WxYu2mPaEHKuTR3Nh5fFtLIFqc51-Bo91yRNaAUUf4/ck"
    delaunayConfig = defaultDelaunayConfig 8 150
    initPts = Map.fromList $
      [(toV p, Just True)  | p <- initAllowed] ++
      [(toV p, Just False) | p <- initDisallowed] ++
      [(toV p, Nothing) | p <- initSearch]
      where
        -- initAllowed    = [ (x,y,z) | (x,y,z,isAllowed) <- aabPoints3d,     isAllowed ]
        -- initDisallowed = [ (x,y,z) | (x,y,z,isAllowed) <- aabPoints3d, not isAllowed ]
        -- initSearch = [ (1.1, 2.6, 3.35)  ]
        initAllowed = []
        initDisallowed = []
        initSearch = [ (1.03, 2.55, 2.8 )
                     , (0.9 , 2.55, 2.8 ) , (1.2 , 2.55, 2.8 )
                     , (1.03, 2.2 , 2.8 ) , (1.03, 2.72, 2.8 )
                     , (1.03, 2.55, 2.7 ) , (1.03, 2.55, 2.94)
                     , (1.1, 2.6, 3.35)
                     ]

boundsProgram "QED3Mixed_spectrumTrials_nmax6" =
  local (setJobType (MPIJob 1 18)  . setJobTime (5*hour) . setSlurmPartition "pi_poland" . setJobMemory "90G") $ do
  forConcurrently_ coords $
    \c -> do
      searchStatus <- checkBoundOutput $ Bound.remoteCompute $ bound (toV c)
      DB.insert delaunaySearchPoints (toV @2 c) (Just searchStatus)
      Log.info "Finished point" ( fromRational @Double (fst c)
                                , fromRational @Double (snd c)
                                , searchStatus)
  where
    nmax = 6
    bound deltaExts = Bound
      { boundKey = q3mFeasibleDefaultGaps deltaExts nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    coords = filter (/=(0.9,2.8)) [ (x,y) | x <- [0.9, 1.1, 1.3, 1.5]
                                          , y <- [2.2, 2.4, 2.5, 2.6, 2.7, 2.8] ]

boundsProgram "QED3Mixed_m1binary_nmax6" =
  local (setJobType (MPIJob 1 28)  . setJobTime (2*hour) . setJobMemory "90G"
         . setSlurmPartition "pi_poland") $ do
  checkpointMap <- newQED3CheckpointMap affine Nothing
  forConcurrently_ [1,2,5,8,9] $ \x -> Bound.remoteCompute (bound x) >>= Log.info (pack $ "finished " ++ show x)
  --lambdaMap     <- newQED3LambdaMap affine Nothing
  bsResult <- BS.binarySearchPersistent (DB.KeyValMap "binarySearchPoints") bsConfig
              (checkBoundOutput . Bound.remoteComputeWithCheckpointMap checkpointMap . bound)
  Log.info "Final search bracket" bsResult
  where
    nmax = 6
    bound dm1 = Bound
      { boundKey = q3mFeasibleCutM1Gaps dm1 nmax
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    bsConfig = BS.BinarySearchConfig
      { initialBracket = BS.Bracket 1 9
      , threshold      = 0.01
      , terminateTime  = Nothing
      }
    affine = AffineTransform
      { affineShift  = toV (1.3, 2.5)
      , affineLinear = toV ( toV (0.2, 0  )
                           , toV (0   , 0.3)
                           )
      }

boundsProgram p = unknownProgram p
