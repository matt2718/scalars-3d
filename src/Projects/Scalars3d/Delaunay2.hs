{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeInType          #-}

module Projects.Scalars3d.Delaunay2 where

import           Control.Applicative                 ((<|>))
import           Control.Monad.Trans.Maybe           (MaybeT (..), runMaybeT)
import           Data.Map                            (Map)
import qualified Data.Map                            as Map
import           GHC.TypeNats                        (KnownNat)
import qualified Bootstrap.Math.AffineTransform  as AT
import           Hyperion
import qualified Hyperion.Bootstrap.PointCloudSearch as PC
import           Hyperion.Bootstrap.DelaunaySearch
import qualified Hyperion.Database                   as DB
import           Hyperion.ExtVar                     (newExtVarStream)
import qualified Hyperion.Log                        as Log
import           Linear.V                            (V)
import           Linear.Vector                       (zero)
import           Bootstrap.Math.Linear.Util          ()

delaunaySearchCubePersistent
  :: forall n . ( KnownNat n )
  => DB.KeyValMap (V n Rational) (Maybe Bool)
  -> DelaunayConfig
  -> AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> Cluster Bool)
  -> Cluster PC.PointCloudStatus
delaunaySearchCubePersistent results delaunayCfg affine initialPts f = do
  (eVar, getExtStream) <- newExtVarStream []
  Log.info "ExtVar for delaunaySearch" eVar
  let
    newPts = Map.fromList [ (AT.apply affine x, Nothing)
                          | x <- zero : PC.duoCubeCentered ]
    allPts = Map.union initialPts newPts
    pointCloudCfg = PC.PointCloudConfig
      { PC.nThreads        = nThreads delaunayCfg
      , PC.nSteps          = const (nSteps delaunayCfg)
      , PC.getNextPoint    = \pointMap -> runMaybeT $
                                          MaybeT getExtStream <|>
                                          MaybeT (delaunaySearchPoint delaunayCfg pointMap)
      , PC.addBoundingCube = False
      , PC.terminateTime   = terminateTime delaunayCfg
      }
  PC.pointCloudSearchRegionPersistent results pointCloudCfg affine allPts f
