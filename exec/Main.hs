module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.Scalars3d.IsingSigEpsTest2020
import qualified Projects.Scalars3d.SingletScalar2020
import qualified Projects.Scalars3d.SingletScalarBlocks3d2020
import qualified Projects.Scalars3d.ONVec2021
import qualified Projects.Scalars3d.QED3Single2022
import qualified Projects.Scalars3d.QED3Mixed2022

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [ Projects.Scalars3d.IsingSigEpsTest2020.boundsProgram
  , Projects.Scalars3d.SingletScalar2020.boundsProgram
  , Projects.Scalars3d.SingletScalarBlocks3d2020.boundsProgram
  , Projects.Scalars3d.ONVec2021.boundsProgram
  , Projects.Scalars3d.QED3Single2022.boundsProgram
  , Projects.Scalars3d.QED3Mixed2022.boundsProgram
  ]
